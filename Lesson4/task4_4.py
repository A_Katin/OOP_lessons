# Опишіть свій клас винятку. Напишіть функцію, яка викидатиме цей виняток, якщо користувач введе певне значення,
# і перехопіть цей виняток під час виклику функції.
from string import digits


class MyClassException(AttributeError):
    ''' My first class of exception '''
    pass


def is_include_number(code):
    for digit in digits:
        if digit in code:
            return True
    return False


def check_code(code):
    if not isinstance(code, str):
        raise MyClassException('Code must be string type')
    if len(code) < 17:
        raise MyClassException('Code must be 17 characters long')
    if not is_include_number(code):
        raise MyClassException('Code must include digits')
    print(f'Code "{code}" is valid')


def main():
    codes_list = ['WA1DECF37M1598201', 58821136647785, '5J8TC2H82NL006061', 'WAYDIDYTYTDGHVBTP',
                  'JTDKAMFP6M3HOBJO7', 'WA1DECF37M1598201']
    for elem in codes_list:
        try:
            check_code(elem)
        except MyClassException as msg:
            print(msg)

    code_from_input_keyboard = input('\nPlease, input your code for validation: ')
    try:
        check_code(code_from_input_keyboard)
    except MyClassException as msg:
        print(msg)


if __name__ == '__main__':
    main()
