# Опишіть клас співробітника, який вміщує такі поля: ім'я, прізвище, відділ і рік початку роботи.
# Конструктор має генерувати виняток, якщо вказано неправильні дані. Введіть список працівників із клавіатури.
# Виведіть усіх співробітників, які були прийняті після 2021 року.
import time
from string import digits


class Employee:
    DEPARTMENTS = 'IT', 'Marketing', 'Suply', 'HR', 'Sales'
    ID_COUNT = 0
    DICT_EMPLOYEES = {}

    def __init__(self, first_name, last_name, department, date_of_start_work):
        if not isinstance(first_name, str):
            raise TypeError('\nFirst name must be string type')
        if self.is_include_number(first_name):
            raise ValueError('\nFirst name must not include digits')
        self.first_name = first_name.capitalize()
        if not isinstance(last_name, str):
            raise TypeError('\nLast name must be string type')
        if self.is_include_number(last_name):
            raise ValueError('\nLast name must not include digits')
        self.last_name = last_name.capitalize()
        if department not in self.DEPARTMENTS:
            raise ValueError('\nDepartament must be selected from the given items')
        self.department = department
        try:
            self.date_of_start_work = time.strptime(date_of_start_work, '%d/%m/%Y')
            Employee.ID_COUNT += 1
            if Employee.DICT_EMPLOYEES.get(Employee.ID_COUNT) is None:
                Employee.DICT_EMPLOYEES[Employee.ID_COUNT] = self
                print(f'Employee {self.first_name} {self.last_name} was successfully added to database')
        except ValueError as msg:
            print(msg)

    @staticmethod
    def is_include_number(code):
        for digit in digits:
            if digit in code:
                return True
        return False

    def __str__(self):
        return '\n{:_^40}'.format(f' Fullname: {self.first_name} {self.last_name} ') + \
               '{:40}'.format(f'\nDepartment: {self.department}') + \
               '{:40}'.format(f'\nDate of start working: {self.date_of_start_work.tm_mday:02}/'
                              f'{self.date_of_start_work.tm_mon:02}/'
                              f'{self.date_of_start_work.tm_year}')


def add_employee_to_database():
    try:
        print('\nPlease be careful when entering employee information!')
        print('Adding a new employee...')
        return Employee(
            input('Input first name: '),
            input('Input last name: '),
            input(f'Enter a department from the specified list\n'
                  f' {Employee.DEPARTMENTS}:\n'),
            input('Date of start working (dd/mm/yyyy): ')
        )
    except (TypeError, ValueError) as msg:
        print(msg)


def main():
    employee_1 = Employee('Jonh', 'Cleese', 'Marketing', '31/03/2022')
    employee_2 = Employee('Alice', 'Merton', 'HR', '11/11/2021')
    employee_3 = Employee('Kevin', "O'Brien", 'IT', '05/12/2021')
    employee_4 = Employee('Nick', 'Glancy', 'Suply', '01/03/2022')

    # employee's who's input from keyboard
    employee_5 = add_employee_to_database()
    employee_6 = add_employee_to_database()

    temp_dict_of_employees = {}
    for x in Employee.DICT_EMPLOYEES:
        if Employee.DICT_EMPLOYEES[x].date_of_start_work.tm_year > 2021:
            temp_date = Employee.DICT_EMPLOYEES[x].date_of_start_work
            temp_key_value = f'{temp_date.tm_mday:02}/{temp_date.tm_mon:02}/{temp_date.tm_year}'
            temp_dict_of_employees[temp_key_value] = Employee.DICT_EMPLOYEES[x]
    if len(temp_dict_of_employees) > 0:
        print('\n\n{:*^45}'.format(f' List of employees hired after 2021 '))
        for y in sorted(temp_dict_of_employees.keys()):
            print(temp_dict_of_employees[y], sep='\n')


if __name__ == '__main__':
    main()
