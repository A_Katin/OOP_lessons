# Створіть програму спортивного комплексу, у якій передбачене меню: 1 - перелік видів спорту, 2 - команда тренерів,
# 3 - розклад тренувань, 4 - вартість тренування. Дані зберігати у словниках.
# Також передбачити пошук по прізвищу тренера, яке вводиться з клавіатури у відповідному пункті меню.
# Якщо ключ не буде знайдений, створити відповідний клас-Exception, який буде викликатися в обробнику виключень.
import time
from datetime import datetime, timedelta
from string import digits
from pyfiglet import Figlet


class IncorrectInputData(AttributeError):
    pass


class TimeTable:
    def __init__(self, time_work_start=None, time_work_end=None, time_weekend_start=None, time_weekend_end=None):
        if time_work_start:
            self.time_work_start = time.strptime(time_work_start, "%H:%M")
        else:
            self.time_work_start = time_work_start
        if time_work_end:
            self.time_work_end = time.strptime(time_work_end, "%H:%M")
        else:
            self.time_work_end = time_work_end
        if time_weekend_start:
            self.time_weekend_start = time.strptime(time_weekend_start, "%H:%M")
        else:
            self.time_weekend_start = time_weekend_start
        if time_weekend_end:
            self.time_weekend_end = time.strptime(time_weekend_end, "%H:%M")
        else:
            self.time_weekend_end = time_weekend_end

    def __str__(self):
        return f'\nРаботаем:\n' \
               f'\tПн - Пт\t\t{self.time_work_start.tm_hour:02}:{self.time_work_start.tm_min:02} - ' \
               f'{self.time_work_end.tm_hour:02}:{self.time_work_end.tm_min:02}\n' \
               f'\tСб - Вс\t\t{self.time_weekend_start.tm_hour:02}:{self.time_weekend_start.tm_min:02} - ' \
               f'{self.time_weekend_end.tm_hour:02}:{self.time_weekend_end.tm_min:02}\n'

    def trainer_timetable(self):
        f_time_work = ''
        if self.time_work_start is not None and self.time_work_end is not None:
            f_time_work = f'\tПн - Пт\t\t{self.time_work_start.tm_hour:02}:{self.time_work_start.tm_min:02} - ' \
                          f'{self.time_work_end.tm_hour:02}:{self.time_work_end.tm_min:02}\n'
        f_weekend_work = ''
        if self.time_weekend_start is not None and self.time_weekend_end is not None:
            f_weekend_work = f'\tСб - Вс\t\t{self.time_weekend_start.tm_hour:02}:{self.time_weekend_start.tm_min:02} - ' \
                             f'{self.time_weekend_end.tm_hour:02}:{self.time_weekend_end.tm_min:02}\n'
        return f'Время работы в клубе:\n' \
               f'{f_time_work}' \
               f'{f_weekend_work}'


class Trainer:
    CATEGORIES = {'A': ['Персональный тренер', 500],
                  'B': ['Тренер высшей категории', 800],
                  'C': ['Мастер-тренер', 1200]}
    TRAINERS_DICT = {}
    ID_COUNT = 0

    def __init__(self, first_name, last_name, category, sports):
        if not isinstance(first_name, str):
            raise IncorrectInputData('\nFirst name must be string type')
        if self.is_include_number(first_name):
            raise IncorrectInputData('\nFirst name must not include digits')
        self.first_name = first_name.capitalize()
        if not isinstance(last_name, str):
            raise IncorrectInputData('\nLast name must be string type')
        if self.is_include_number(last_name):
            raise IncorrectInputData('\nLast name must not include digits')
        self.last_name = last_name.capitalize()
        if category not in self.CATEGORIES:
            raise IncorrectInputData('\nСategory must be selected from the given items')
        self.category = category
        if not all(x in FitnessClub.SPORTS for x in sports):
            raise IncorrectInputData('Incorrect kinds of sports')
        self.sports = sports
        Trainer.ID_COUNT += 1
        if Trainer.TRAINERS_DICT.get(Trainer.ID_COUNT) is None:
            Trainer.TRAINERS_DICT[Trainer.ID_COUNT] = self

    @staticmethod
    def is_include_number(code):
        for digit in digits:
            if digit in code:
                return True
        return False

    def __str__(self):

        return '\n{:_^50}'.format(f' Тренер: {self.first_name} {self.last_name} ') + \
               '{:50}'.format(f'\nКатегория: {Trainer.CATEGORIES.get(self.category)[0]}') + \
               '{:50}'.format(f'\nСпециализация: {", ".join(self.sports)}')

    def __repr__(self):
        return '{:^40}'.format(f' {self.first_name} {self.last_name} ')


class FitnessClub:
    DICT_CLUBS = {}
    ID_COUNT = 0
    # all available kinds of sports for all clubs
    SPORTS = ['Stretching', 'Gym', 'Toned Arms', 'Pilates', 'Box', 'Core workout', 'HOT Yoga', 'Squash', 'Aqua fitness']

    def __init__(self, name, sports, address, phones, timetable, rate):
        if not isinstance(name, str):
            raise IncorrectInputData('\nName must be string type')
        if self.is_include_number(name):
            raise IncorrectInputData('\nName must not include digits')
        self.name = name
        if not all(x in FitnessClub.SPORTS for x in sports):
            raise IncorrectInputData('Incorrect kinds of sports')
        self.sports = sports
        self.trainers = []
        self.address = address
        self.phones = phones
        if not isinstance(rate, float):
            raise IncorrectInputData('\nName must be float type')
        self.rate = rate
        try:
            self.timetable_club = timetable
            FitnessClub.ID_COUNT += 1
            if FitnessClub.DICT_CLUBS.get(FitnessClub.ID_COUNT) is None:
                FitnessClub.DICT_CLUBS[FitnessClub.ID_COUNT] = self
        except IncorrectInputData as msg:
            print(msg)

    def __str__(self):
        return '{:^30}'.format(f' Fitness Club Champion "{self.name}" ')

    def __repr__(self):
        return '{:^30}'.format(f' {self.name} ')

    def add_trainer(self, trainer, trainer_timetable):
        self.trainers.append([trainer, trainer_timetable])

    @staticmethod
    def is_include_number(code):
        for digit in digits:
            if digit in code:
                return True
        return False

    @staticmethod
    def main_info_menu():
        print('{:@^59}'.format(f'@') + '\n{:*^59}\n'.format(f' KYIV '))
        for key, value in FitnessClub.DICT_CLUBS.items():
            print(f'{key}. {value}')
        print('\n{:20}'.format(f'\t(S or Exit). Выход'))

    @staticmethod
    def club_main_info_menu(club):
        print('{:@^60}'.format(f' Fitness Club Champion "{club.name}" ') +
              '\n{:*^60}\n'.format('*') +
              '\n{:25}'.format(f'1. Виды спорта') + '{:>15}'.format(f'\t4. Расписание занятий') +
              '\n{:25}'.format(f'2. Команда тренеров') + '{:>15}'.format(f'\t5. Стоимость тренировок') +
              '\n{:25}'.format(f'3. Контактная информация') + '{:>11}'.format(f'6. Назад')
              )

    @staticmethod
    def trainers_info_menu(club):
        print('{:@^60}'.format(f' Команда тренеров Fitness Club Champion "{club.name}" ') +
              '\n{:*^60}\n'.format('*') +
              '\n{:30}'.format(f'1. Тренеры по категории\t\t') + '{:>20}'.format(f'3. Тренеры по виду спорта') +
              '\n{:30}'.format(f'2. Поиск тренера\t') + '{:10}'.format(f'4. Назад')
              )


def main():
    num_todo = ''
    preview_text = Figlet(font='digital')
    print('\n', preview_text.renderText('Fitness Club Champion Network'))
    club_1 = FitnessClub('Печерский',
                         ['Stretching', 'Gym', 'Toned Arms', 'Pilates', 'HOT Yoga', 'Squash', 'Aqua fitness'],
                         'г. Киев, ул. Зверинецкая, 59, ЖК «Триумф»',
                         '+38 (044) 585-99-09',
                         TimeTable('7:00', '21:00', '9:00', '21:00'),
                         2.00
                         )
    club_2 = FitnessClub('Дарницкий',
                         ['Stretching', 'Gym', 'Toned Arms', 'Pilates', 'Box', 'HOT Yoga', 'Aqua fitness'],
                         'г. Киев, ул. Регенераторная, 4б, ЖК «Комфорт Таун»',
                         '+38 (044) 593-95-55',
                         TimeTable('8:00', '21:00', '9:00', '21:00'),
                         1.00
                         )
    club_3 = FitnessClub('Оболонский',
                         ['Stretching', 'Gym', 'Toned Arms', 'Pilates', 'Box', 'Core workout', 'HOT Yoga'],
                         'г. Киев, просп. Степана Бандери, 34в, РЦ «Блокбастер»',
                         '+38 (044) 222-4-222',
                         TimeTable('8:00', '22:00', '9:00', '21:00'),
                         1.50
                         )
    club_1.add_trainer(Trainer('Александр', 'Горбач', 'A', ['Gym', 'Toned Arms', 'Core workout']),
                       TimeTable('09:00', '18:00', '11:00', '17:00'))
    club_1.add_trainer(Trainer('Оксана', 'Любимова', 'B', ['HOT Yoga', 'Aqua fitness']),
                       TimeTable('12:00', '18:00', '09:00', '15:00'))
    club_1.add_trainer(Trainer('Яна', 'Барановская', 'B', ['Aqua fitness', 'Squash']),
                       TimeTable('7:00', '14:00'))
    club_1.add_trainer(Trainer('Татьяна', 'Савченко', 'A', ['Aqua fitness', 'Squash']),
                       TimeTable('14:00', '21:00'))
    club_1.add_trainer(Trainer('Стелла', 'Журавель', 'C', ['Aqua fitness', 'Squash']),
                       TimeTable('7:00', '12:00', '15:00', '21:00'))
    club_1.add_trainer(Trainer('Ксения', 'Ковальчук', 'A', ['Stretching', 'Gym', 'Pilates']),
                       TimeTable('09:00', '18:00', '10:00', '13:00'))
    club_1.add_trainer(Trainer('Владимир', 'Фомин', 'C', ['Aqua fitness', 'Gym']),
                       TimeTable('07:00', '12:00', '09:00', '13:00'))
    # trainers club_2
    club_2.add_trainer(Trainer('Игорь', 'Радионов', 'A', ['Box', 'Toned Arms']),
                       TimeTable('09:00', '18:00', '10:00', '13:00'))
    club_2.add_trainer(Trainer('Максим', 'Юхименко', 'C', ['Gym', 'Toned Arms', 'Core workout']),
                       TimeTable('15:00', '21:00'))
    club_2.add_trainer(Trainer('Ярослав', 'Карпов', 'C', ['HOT Yoga', 'Stretching', 'Gym', 'Pilates']),
                       TimeTable('12:30', '19:00', '17:00', '21:00'))
    club_2.add_trainer(Trainer('Александр', 'Дубов', 'A', ['Gym', 'Toned Arms']),
                       TimeTable('15:00', '21:00', '09:00', '15:00'))
    club_2.add_trainer(Trainer('Александр', 'Лавров', 'A', ['Gym', 'Toned Arms']),
                       TimeTable('8:00', '16:00', '15:00', '21:00'))
    club_2.add_trainer(Trainer('Владислава', 'Таран', 'A', ['Stretching', 'Gym', 'Pilates']),
                       TimeTable('09:00', '18:00', '11:00', '17:00'))
    # trainers club_3
    club_3.add_trainer(Trainer('Виктория', 'Юркова', 'B', ['Stretching', 'Gym', 'Pilates']),
                       TimeTable('09:00', '18:00', '10:00', '13:00'))
    club_3.add_trainer(Trainer('Елена', 'Казакевич', 'B', ['HOT Yoga', 'Aqua fitness', 'Stretching', 'Gym', 'Pilates']),
                       TimeTable('08:00', '15:00', '18:00', '22:00'))
    club_3.add_trainer(Trainer('Тамара', 'Савченко', 'C', ['HOT Yoga', 'Aqua fitness', 'Stretching', 'Gym', 'Pilates']),
                       TimeTable('15:00', '22:00', '09:00', '12:00'))
    club_3.add_trainer(Trainer('Людмила', 'Хоменко', 'A', ['Stretching', 'Gym', 'Toned Arms', 'Pilates', 'HOT Yoga']),
                       TimeTable('12:30', '19:00', '17:00', '22:00'))
    club_3.add_trainer(Trainer('Алиса', 'Попова', 'B', ['HOT Yoga', 'Stretching', 'Gym', 'Pilates']),
                       TimeTable('12:00', '18:00', '12:00', '18:00'))
    club_3.add_trainer(Trainer('Иван', 'Канапка', 'C', ['Aqua fitness', 'Gym']),
                       TimeTable('8:00', '14:00', '09:00', '15:00'))
    club_3.add_trainer(Trainer('Владимир', 'Фомин', 'C', ['Aqua fitness', 'Gym']),
                       TimeTable('14:00', '22:00', '15:00', '22:00'))
    club_3.add_trainer(Trainer('Максим', 'Юхименко', 'C', ['Gym', 'Toned Arms', 'Core workout']),
                       TimeTable('08:00', '14:00'))

    while num_todo != 'Exit':
        FitnessClub.main_info_menu()
        num_todo = str(input('\nВыберите действие: '))
        match num_todo:
            case '1' | '2' | '3' | '4' | '5' | '6' | '7':  # for the future
                selected_club = FitnessClub.DICT_CLUBS.get(int(num_todo))
                num_point = 0
                while num_point != 6:
                    print('\n', preview_text.renderText('Fitness Club Champion Network'))
                    FitnessClub.club_main_info_menu(selected_club)
                    num_point = int(input('\nВыберите действие: '))
                    match num_point:
                        case 1:
                            print('\n', preview_text.renderText('Fitness Club Champion Network'))
                            print('{:@^50}'.format(f' В нашем Fitness Club Champion "{selected_club.name}" ') +
                                  '\n{:^50}\n'.format('доступны занятия следующими видами спорта:'))
                            print('\n'.join(selected_club.sports))
                        case 2:
                            num_point2_4 = 0
                            while num_point2_4 != 4:
                                print('\n', preview_text.renderText('Fitness Club Champion Network'))
                                FitnessClub.trainers_info_menu(selected_club)
                                num_point = int(input('\nВыберите действие: '))
                                match num_point:
                                    case 1:  # club trainers sorted by category
                                        temp_dict = {}
                                        for el in selected_club.trainers:
                                            if temp_dict.get(el[0].category, []):
                                                temp_dict[el[0].category].append(el)
                                            else:
                                                temp_dict[el[0].category] = [el]
                                        if len(temp_dict) > 0:
                                            temp_dict = dict(
                                                sorted(temp_dict.items(), reverse=True, key=lambda x: x[0]))
                                            for x in temp_dict.values():
                                                for y in x:
                                                    print(y[0])  # instance Trainer
                                                    print(TimeTable.trainer_timetable(
                                                        y[1]))  # instance trainer's TimeTable
                                    case 2:  # find club trainer
                                        temp_dict = {}
                                        tren = input('Введите фамилию тренера: ').strip().lower()
                                        for el in selected_club.trainers:
                                            if temp_dict.get(el[0].last_name.lower(), []):  # el[0] instance Trainer
                                                temp_dict[el[0].last_name.lower()].append(el)
                                            else:
                                                temp_dict[el[0].last_name.lower()] = [el]
                                        try:
                                            temp_key_dict = temp_dict[tren.lower()]
                                            print(f'... найдены следующие тренеры с такой фамилией:')
                                            for y in temp_key_dict:
                                                print(y[0])  # instance Trainer
                                                print(TimeTable.trainer_timetable(
                                                    y[1]))  # instance trainer's TimeTable
                                        except KeyError as msg:
                                            raise IncorrectInputData(
                                                f'Тренер клуба с такой фамилией не найден ({msg})')
                                        # finally:
                                        #     print('\n', preview_text.renderText('Fitness Club Champion Network'))
                                        #     FitnessClub.trainers_info_menu(selected_club)
                                        #     num_point = int(input('\nВыберите действие: '))

                                        # if tren.lower() not in temp_dict:
                                        #     raise IncorrectInputData(
                                        #         'Тренер клуба с такой фамилией не найден') from None
                                        # else:
                                        #     print(f'... найдены следующие тренеры с такой фамилией:')
                                        #     for y in temp_dict[tren.lower()]:
                                        #         print(y[0])  # instance Trainer
                                        #         print(TimeTable.trainer_timetable(
                                        #             y[1]))  # instance trainer's TimeTable
                                    case 3:  # club trainers sorted by kind of sports
                                        print('\n{:@^50}'.format(
                                            f' В нашем Fitness Club Champion "{selected_club.name}" ') +
                                              '\n{:^50}\n'.format(
                                                  'для занятия следующими видами спорта есть квалифицированные тренеры:'))
                                        for sport in selected_club.sports:
                                            temp_dict = {}
                                            for el in selected_club.trainers:
                                                if sport in el[0].sports:
                                                    if temp_dict.get(sport, []):
                                                        temp_dict[sport].append(el)
                                                    else:
                                                        temp_dict[sport] = [el]
                                            if len(temp_dict) > 0:
                                                print('{:@^50}'.format(' ' + sport + ' '))
                                                for x in temp_dict.values():
                                                    for y in x:
                                                        print(y[0])  # instance Trainer
                                                        print(TimeTable.trainer_timetable(
                                                            y[1]))  # instance trainer's TimeTable

                                    case 4:
                                        break
                                    case _:
                                        raise IncorrectInputData
                        case 3:
                            print('\n', preview_text.renderText('Fitness Club Champion Network'))
                            print('{:@^50}'.format(f' Fitness Club Champion "{selected_club.name}" ') +
                                  '\n{:^50}\n'.format('находится по адресу:') +
                                  '\n{:50}'.format(selected_club.address) +
                                  '\n{:50}'.format(selected_club.phones))
                            print(selected_club.timetable_club)
                        case 4:
                            week_by_date = []
                            for x in range(1 - datetime.now().isoweekday(), 8 - datetime.now().isoweekday()):
                                week_by_date.append(datetime.now() + timedelta(days=x))
                            print('\n{:@^126}\n'.format(
                                f' Расписание групповых занятий в Fitness Club Champion "{selected_club.name}"'))
                            print('|' + '{:^14}'.format(' ') + '|', end='')
                            for day in week_by_date:
                                print('|' + '{:^14}'.format(day.strftime('%A')) + '|', end='')
                            print()
                            print('|' + '{:_^14}'.format('_') + '|', end='')
                            for day in week_by_date:
                                print('|' + '{:_^14}'.format(day.strftime('%d/%m/%Y')) + '|', end='')
                            print()
                            for hour in range(selected_club.timetable_club.time_work_start.tm_hour,
                                              selected_club.timetable_club.time_work_end.tm_hour):
                                if 'Aqua fitness' in selected_club.sports and hour in [7, 8, 9, 10, 11, 17, 18, 19, 20]:
                                    print('|' + '{:^14}'.format(' ') +
                                          '|' + (('|' + '{:^14}'.format('Aqua fitness') + '|') * 7), end='')
                                    ts = time.strptime(str(hour) + ':00', "%H:%M")
                                    print('\n|' + '{:^14}'.format(f'{ts.tm_hour:02}:{ts.tm_min:02}') + '|' + (
                                            ('|' + '{:^14}'.format(' 55 мин ') + '|') * 7))
                                elif hour in [7, 8, 9, 10, 11, 17, 18, 19, 20, 21]:
                                    print('|' + '{:^14}'.format(' ') +
                                          '|' + (('|' + '{:^14}'.format('Stretching') + '|') * 7), end='')
                                    ts = time.strptime(str(hour) + ':00', "%H:%M")
                                    print('\n|' + '{:^14}'.format(f'{ts.tm_hour:02}:{ts.tm_min:02}') + '|' + (
                                            ('|' + '{:^14}'.format(' 45 мин ') + '|') * 7))
                                elif 'HOT Yoga' in selected_club.sports and hour in [12, 13, 14]:
                                    print('|' + '{:^14}'.format(' ') +
                                          '|' + (('|' + '{:^14}'.format('HOT Yoga') + '|') * 7), end='')
                                    ts = time.strptime(str(hour) + ':00', "%H:%M")
                                    print('\n|' + '{:^14}'.format(f'{ts.tm_hour:02}:{ts.tm_min:02}') + '|' + (
                                            ('|' + '{:^14}'.format(' 45 мин ') + '|') * 7))
                                elif 'Pilates' in selected_club.sports and hour in [15, 16]:
                                    print('|' + '{:^14}'.format(' ') +
                                          '|' + (('|' + '{:^14}'.format('Pilates') + '|') * 7), end='')
                                    ts = time.strptime(str(hour) + ':00', "%H:%M")
                                    print('\n|' + '{:^14}'.format(f'{ts.tm_hour:02}:{ts.tm_min:02}') + '|' + (
                                            ('|' + '{:^14}'.format(' 55 мин ') + '|') * 7))
                                else:
                                    print('|' + '{:^14}'.format(' ') +
                                          '|' + (('|' + '{:^14}'.format(' ') + '|') * 7), end='')
                                    ts = time.strptime(str(hour) + ':00', "%H:%M")
                                    print('\n|' + '{:^14}'.format(f'{ts.tm_hour:02}:{ts.tm_min:02}') + '|' + (
                                            ('|' + '{:^14}'.format(' ') + '|') * 7))
                                print(('|' + '{:_^14}'.format('_') + '|') * 8)
                        case 5:
                            print('\n', preview_text.renderText('Fitness Club Champion Network'))
                            print('{:@^60}'.format(f' В нашем Fitness Club Champion "{selected_club.name}" ') +
                                  '\n{:^60}\n'.format('установлена следующая стоимость (грн/ч) занятий с тренером:') +
                                  '{:!^60}\n'.format('при покупке > 10 занятий предоставляется скидка 50%'))
                            for el in Trainer.CATEGORIES.values():
                                print('{:30}'.format(f'\t- {el[0]}') +
                                      '\t{:>10}'.format(f'{el[1] * selected_club.rate:.2f}'))
                        case 6:
                            break
                        case _:
                            raise IncorrectInputData
            case ('S' | 'Exit' | 's'):
                break
            case _:
                raise IncorrectInputData


if __name__ == '__main__':
    main()
