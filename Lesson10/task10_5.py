# З клавіатури вводиться рядок, в якому є інформація про прізвище, ім'я, дату народження, електронну адресу та
# відгук про курси учня. Написати функцію, яка, використовуючи регулярні вирази, витягне дані з рядка і поверне словник.
import re
from collections import defaultdict
import pprint


def text_analise(string):
    result_dict = defaultdict(list)
    string = re.sub(r'[:!;]+', '', string)
    names = re.compile(r'([A-ZА-Я][a-zа-я]+)([A-ZА-Я][a-zа-я]+)')
    hb = re.compile(r'\d{2}[\-/\.]\d{2}[\-/\.]\d{4}')
    e_mail = re.compile(r'\w+@\w+[\.\w]+[\.\w]*')
    # из условия задания не очень понятно как определять отзыв о курсах учащегося
    # либо вариант, что от слова отзыв и до конца строки
    vidguk = re.compile(r'(?=отзыв).*?$')
    # либо вариант, от эл. адреса и до конца строки
    temp_str = re.search(r'\w+@\w+[\.\w]+[\.\w]*', string)
    if temp_str:
        email_len = temp_str.end() - temp_str.start() - 1
        pattern_str = '(?<=@[\.\w]{'+str(email_len)+'}).*?$'
        pattern_str = r'{}'.format(pattern_str)
        vidguk = re.compile(pattern_str)
    temp_lst = names.findall(string)
    if len(temp_lst) > 0:
        for el in temp_lst:
            result_dict['student'].append(el)
    temp_lst = hb.findall(string)
    if len(temp_lst) > 0:
        for el in temp_lst:
            result_dict['student'].append(el)
    temp_lst = e_mail.findall(string)
    if len(temp_lst) > 0:
        for el in temp_lst:
            result_dict['student'].append(el)
    temp_lst = vidguk.findall(string)
    if len(temp_lst) > 0:
        for el in temp_lst:
            result_dict['student'].append(el)
    return result_dict


def main():
    input_text = input('Введите текст для обработки: ')
    result = text_analise(input_text)
    pprint.pprint(result)


if __name__ == '__main__':
    main()
