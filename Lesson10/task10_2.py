# Написати функцію, яка за допомогою регулярних виразів з файлу витягує дані про дату народження,
# телефон та електронну адресу. Дані потрібно записати до іншого файлу.
import re
from datetime import datetime


def validate_data(date_from_text, char_razd):
    try:
        if date_from_text != datetime.strptime(date_from_text, '%d' + char_razd + '%m' + char_razd + '%Y').strftime(
                '%d' + char_razd + '%m' + char_razd + '%Y'):
            raise ValueError
        return True
    except ValueError:
        # raise ValueError ("Ошибка - это не формат даты")
        return False


def line_parser():
    '''
    #
    # Регулярные выражения
    # .	Один любой символ, кроме новой строки \n.
    # ?	0 или 1 вхождение шаблона слева
    # +	1 и более вхождений шаблона слева
    # *	0 и более вхождений шаблона слева
    # \w	Любая цифра или буква (\W — все, кроме буквы или цифры)
    # \d	Любая цифра [0-9] (\D — все, кроме цифры)
    # \s	Любой пробельный символ (\S — любой непробельный символ)
    # \b	Граница слова
    # [..]	Один из символов в скобках ([^..] — любой символ, кроме тех, что в скобках)
    # \	Экранирование специальных символов (\. означает точку или \+ — знак «плюс»)
    # ^ и $	Начало и конец строки соответственно
    # {n,m}	От n до m вхождений ({,m} — от 0 до m)
    # a|b	Соответствует a или b
    # ()	Группирует выражение и возвращает найденный текст
    # \t, \n, \r	Символ табуляции, новой строки и возврата каретки соответственно
    # sd*    s, затем ноль или несколько d
    # sd+       s, затем одна или несколько d
    # sd?       s, затем ноль или одна d
    # sd{3}       s, затем 3 d
    # sd{2, 3}    s, затем от 2 до 3 d
    #
    :return:
    '''
    hb_dict = {}
    hb = re.compile(r'\d{2}[\-/\.]\d{2}[\-/\.]\d{4}')
    hb_month = re.compile(r'\d{2}[\-/\.](\d{2})[\-/\.]\d{4}')
    phone_lst = []
    phone_number = re.compile(r'(?:(?:\+38)?[\- ]?)?(?:\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}')
    email_lst = []
    e_mail = re.compile(r'\w+@\w+[\.\w]+[\.\w]*')
    any_symb = re.compile(r'\W')
    with open('_files/data.txt', 'r', encoding='utf-8') as text_file_in:
        for row in text_file_in:
            # словарь ДР с разбивкой по месяцам
            if len(hb.findall(row)) > 0:
                for el in hb.findall(row):
                    if validate_data(el, re.search(r'\W', el).group()):
                        if hb_dict.get(hb_month.match(el).group(1), []):
                            hb_dict[hb_month.match(el).group(1)].append(
                                datetime.strptime(el, '%d' + re.search(r'\W', el).group() +
                                                  '%m' + re.search(r'\W', el).group() + '%Y'))
                        else:
                            hb_dict[hb_month.match(el).group(1)] = [datetime.strptime(el, '%d' +
                                                                                      re.search(r'\W',el).group() +
                                                                                      '%m' +
                                                                                      re.search(r'\W', el).group() +
                                                                                      '%Y')]
            # список телефонов
            if len(phone_number.findall(row)) > 0:
                for el in phone_number.findall(row):
                    phone_lst.append(el)
            # список адресов электронной почты
            if len(e_mail.findall(row)) > 0:
                for el in e_mail.findall(row):
                    email_lst.append(el)
    with open('_files/final_data_out.txt', 'w', encoding='utf-8') as text_file_out:
        if len(hb_dict) > 0:
            text_file_out.write('__ БЛОК ХЕППИБЁЗДЕЙ __\n')
            for i, y in sorted(hb_dict.items()):
                text_file_out.write(f'{y[0].strftime("%B")}: {[d_.strftime("%d/%m/%Y") for d_ in y]}\n')
        if len(phone_lst) > 0:
            text_file_out.write('__ БЛОК СПИСОК НОМЕРОВ ТЕЛЕФОНОВ __\n')
            for element in phone_lst:
                text_file_out.write(element+'\n')
        if len(email_lst) > 0:
            text_file_out.write('__ БЛОК СПИСОК EMAIL АДРЕСОВ __\n')
            for element in email_lst:
                text_file_out.write(element+'\n')


if __name__ == '__main__':
    line_parser()

