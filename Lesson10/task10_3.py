# Користувач вводить з клавіатури пропозицію. Написати функцію, яка друкуватиме на екран
# останні 3 символи кожного слова.
import re


def main(phrase):
    print('\n'.join(re.findall(r'\w\w\w\b', phrase)))


if __name__ == '__main__':
   main(input('Please input yours magic phrase: '))
