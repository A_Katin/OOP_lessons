# Написати функцію, яка за допомогою регулярних виразів розбиває текст на окремі слова і знаходить частоту окремих слів.
import re
import pprint
from collections import Counter


def get_world_from_phrase(phrase: str) -> dict:
    # при использовании split() в конечном списке оказываются нулевые блоки,
    # что требует дополнительного условия в обработку
    # return {word: phrase.count(word) for word in re.split(r'[\s,\.\-:;\n\r]+', phrase) if len(word) != 0}
    # использование findall() не требует дополнительных условий
    # return {word: phrase.count(word) for word in re.findall(r'[^\s,\.\-:!;]+', phrase)}
    return Counter(re.findall(r'[^\s,\.\-:!;]+', phrase))


if __name__ == '__main__':
    input_text = input('Please input a phrase for word splitting: ')
    words_dict = get_world_from_phrase(input_text)
    # отдельные слова
    pprint.pprint([*words_dict.keys()])
    # частота отдельных слов
    for word, frq in words_dict.most_common():
        print(word, frq)

