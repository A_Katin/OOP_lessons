# Реалізуйте цикл, який перебиратиме всі значення ітерабельного об'єкту iterable
from string import digits
cities = ['New York', 'Kyiv', 'Cairo', 'Mumbai', 'Mexico']
iterable = (f"{index}: {city}" for index, city in enumerate(cities))

while True:
    try:
        print(next(iterable))
    except StopIteration:
        break
