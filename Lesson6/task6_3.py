# Напишіть ітератор, який повертає елементи заданого списку у зворотному порядку (аналог reversed).

def main():
    list_in = input('Введите значения списка через пробел: ').split()

    iterable_1 = (list_in[i] for i in range(len(list_in) - 1, -1, -1))
    while True:
        try:
            print(next(iterable_1))
        except StopIteration:
            break

    print()

    iterable_2 = iter(list_in[::-1])
    while True:
        try:
            print(next(iterable_2))
        except StopIteration:
            break


if __name__ == '__main__':
    main()
