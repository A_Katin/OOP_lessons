# розширте функціональність класу MyList, додавши методи очищення списку, додавання елемента у довільне місце списку,
# видалення елемента з кінця та довільного місця списку.

class MyList(object):
    """Класс списка"""

    class _ListNode(object):
        """Внутренний класс элемента списка"""

        # По умолчанию атрибуты-данные хранятся в словаре __dict__.
        # Если возможность динамически добавлять новые атрибуты
        # не требуется, можно заранее их описать, что более
        # эффективно с точки зрения памяти и быстродействия, что
        # особенно важно, когда создаётся множество экземляров
        # данного класса.
        __slots__ = ('value', 'prev', 'next')

        def __init__(self, value, prev=None, next=None):
            self.value = value
            self.prev = prev
            self.next = next

        def __str__(self):
            return 'MyList._ListNode({}, {}, {})'.format(self.value, id(self.prev), id(self.next))

        def __repr__(self):
            return 'MyList._ListNode({}, {}, {})'.format(self.value, id(self.prev), id(self.next))

    class _Iterator(object):
        """Внутренний класс итератора"""

        def __init__(self, list_instance):
            self._list_instance = list_instance
            self._next_node = list_instance._head

        def __iter__(self):
            return self

        def __next__(self):
            if self._next_node is None:
                raise StopIteration

            value = self._next_node.value
            self._next_node = self._next_node.next

            return value

    def __init__(self, iterable=None):
        # Длина списка
        self._length = 0
        # Первый элемент списка
        self._head = None
        # Последний элемент списка
        self._tail = None

        # Добавление всех переданных элементов
        if iterable is not None:
            for element in iterable:
                self.append(element)

    def append(self, element):
        """Добавление элемента в конец списка"""

        # Создание элемента списка
        node = MyList._ListNode(element)

        if self._tail is None:
            # Список пока пустой
            self._head = self._tail = node
        else:
            # Добавление элемента
            self._tail.next = node
            node.prev = self._tail
            self._tail = node

        self._length += 1

    def clear(self):
        ''' from https://github.com/python/cpython/blob/main/Objects/listobject.c
        static int
        _list_clear(PyListObject *a)
        {
            Py_ssize_t i;
            PyObject **item = a->ob_item;
            if (item != NULL) {
                /* Because XDECREF can recursively invoke operations on
                   this list, we make it empty first. */
                i = Py_SIZE(a);
                Py_SET_SIZE(a, 0);
                a->ob_item = NULL;
                a->allocated = 0;
                while (--i >= 0) {
                    Py_XDECREF(item[i]);
                }
                PyMem_Free(item);
            }
            /* Never fails; the return value can be ignored.
               Note that there is no guarantee that the list is actually empty
               at this point, because XDECREF may have populated it again! */
            return 0;
        }

        /* a[ilow:ihigh] = v if v != NULL.
         * del a[ilow:ihigh] if v == NULL.
         *
         * Special speed gimmick:  when v is NULL and ihigh - ilow <= 8, it's
         * guaranteed the call cannot fail.
         */
         Функция очистки списка, для самой точной работы необходимо получить и очистить все ссылки
          на элементы списка в памяти
        '''
        self._tail = None
        self._head = None
        self._length = 0

    def insert(self, index, element):
        ''' Функция вставки нового элемента по индексу '''
        if not 0 <= index < len(self):
            raise IndexError('list index out of range')

        # получаем отодвигаемый элемент
        node_lst = self._head
        for _ in range(index):
            node_lst = node_lst.next
        # устанавливаем новые ссылки на элементы в зависимости от положения
        if index == 0:
            node_ = MyList._ListNode(element)
            self._head = node_
            self._head.next = node_lst
            node_lst.prev = node_
        else:
            node_ = MyList._ListNode(element)
            node_.next = node_lst
            node_.prev = node_lst.prev
            node_lst.prev.next = node_
            node_lst.prev = node_
        self._length += 1

    def pop(self, index=None):
        ''' Функция удаления элемента списка по его индексу. Если индекс не указан, удаляется последний элемент '''
        if index is None or index == len(self) - 1:
            # работаем с концом списка
            node = self._tail
            node_prev = node.prev
            node_prev.next = None
            node = None
            self._tail = node_prev
        else:
            # получаем удаляемый элемент списка
            node_lst = self._head
            for _ in range(index):
                node_lst = node_lst.next
            if index == 0:
                self._head = node_lst.next
                self._head.prev = None
            else:
                node_prev = node_lst.prev
                node_next = node_lst.next
                node_prev.next = node_next
                node_next.prev = node_prev
            node_lst = None

        self._length -= 1

    def __len__(self):
        return self._length

    def __repr__(self):
        # Метод join класса str принимает последовательность строк
        # и возвращает строку, в которой все элементы этой
        # последовательности соединены изначальной строкой.
        # Функция map применяет заданную функцию ко всем элементам последовательности.
        return 'MyList([{}])'.format(', '.join(map(repr, self)))

    def __getitem__(self, index):
        if not 0 <= index < len(self):
            raise IndexError('list index out of range')

        node = self._head
        for _ in range(index):
            node = node.next

        return node.value

    def __iter__(self):
        return MyList._Iterator(self)


def main():
    # Создание списка
    my_list = MyList([1, 2, 5])

    # Вывод длины списка
    print(len(my_list))

    # Вывод самого списка
    print(my_list)

    print()

    # Обход списка
    for element in my_list:
        print(element)

    my_list.insert(2, "orange")
    print(f'\nТеперь длина списка {len(my_list)}\n')

    # Повторный обход списка
    for element in my_list:
        print(element)

    my_list.pop()
    print(f'\nТеперь длина списка (после удаления последнего элемента) {len(my_list)}\n')
    # Повторный обход списка
    for element in my_list:
        print(element)

    my_list.insert(2, 5)
    print(f'\nТеперь длина списка (вернули элемент) {len(my_list)}\n')
    # Повторный обход списка
    for element in my_list:
        print(element)

    my_list.pop(3)
    print(f'\nТеперь длина списка (после удаления i-го элемента) {len(my_list)}\n')
    # Повторный обход списка
    for element in my_list:
        print(element)

    # очистка списка
    my_list.clear()
    print('\nСписок очищен', my_list)


if __name__ == '__main__':
    main()