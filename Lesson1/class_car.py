# Створіть клас, який описує автомобіль. Створіть клас автосалону, що містить в собі список автомобілів,
# доступних для продажу, і функцію продажу заданого автомобіля.

class Car:
    def __init__(self, brand, model, color, volume):
        self.brand = brand
        self.model = model
        self.color = color
        self.volume = volume

    def __str__(self):
        return f'{self.brand} {self.model}, {self.color}, {self.volume}'

    def __repr__(self):
        return f'{self.brand} {self.model}, {self.color}, {self.volume}'


class CarDealership:
    def __init__(self):
        self.cars = []

    def __str__(self):
        return f'{" | ".join([" ".join(str(item).split("_")) for item in self.cars])}'

    def sell_(self, car, price, sell_out: bool = False):
        if isinstance(car, Car):
            car_id = str(car.brand) + '_' + str(car.model).capitalize() + '_' \
                     + str(car.color).lower() + '_' + str(car.volume)
            if sell_out:
                if car_id in self.cars:
                    self.cars.remove(car_id)
                    print(f'This car {" ".join(car_id.split("_"))} was successfully sold for ${price}')
                else:
                    print(f'This car {" ".join(car_id.split("_"))} not found in price list')
            else:
                car.price = price
                self.cars.append(car_id)
                print(f'This car {" ".join(car_id.split("_"))} was taken for sale')


car_1 = Car('KIA', 'Rio', 'red', '1.6')
car_2 = Car('Buick', 'Regal', 'black', '2.4')
dealer = CarDealership()
dealer.sell_(car_1, 9600)
dealer.sell_(Car('GM', 'RAM', 'white', '6.1'), 96000)
dealer.sell_(car_2, 15000)
print('\n', dealer, '\n')
dealer.sell_(Car('GM', 'RAM', 'white', '6.1'), 110000, True)
dealer.sell_(car_2, 17000, True)
print('')
dealer.sell_(Car('Honda', 'E', 'gray', '210hp'), 21000, True)
