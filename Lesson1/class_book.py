# Створіть клас, який описує книгу. Він повинен містити інформацію про автора, назву, рік видання та жанр.
# Створіть кілька різних книжок. Визначте для нього операції перевірки на рівність та нерівність,
# методи _repr_ та _str_.
# Створіть клас, який описує відгук до книги. Додайте до класу книги поле – список відгуків.
# Зробіть так, щоб при виведенні книги на екран за допомогою функції print також виводилися відгуки до неї.

class Book:
    def __init__(self, author, name, year, genre):
        self.author = author
        self.name = name
        self.year = year
        self.genre = genre
        self.feedbacks = []

    def __str__(self):
        if len(self.feedbacks) != 0:
            return f'name is "{self.name}", author is {self.author}, year is {self.year}, genre is {self.genre}, ' \
                   f'\nFeedbacks: {", ".join([str(item) for item in self.feedbacks])}'
        else:
            return f'name is "{self.name}", author is {self.author}, year is {self.year}, genre is {self.genre}'

    def __repr__(self):
        return f'name is "{self.name}", author is {self.author}, year is {self.year}, genre is {self.genre}'

    def __eq__(self, other):
        if not isinstance(other, Book):
            raise TypeError(f"type of {other} is {type(other)}, needs type Book")
        return self.name == other.name and self.author == other.author

    def __ne__(self, other):
        if not isinstance(other, Book):
            raise TypeError(f"type of {other} is {type(other)}, needs type Book")
        return self.name != other.name or self.author != other.author

    def add_feedback(self, feedback_text):
        self.feedbacks.append(Feedbacks(feedback_text))


class Feedbacks:
    def __init__(self, feedback_text):
        self.feedback = feedback_text

    def __str__(self):
        return f'{self.feedback}'

    def __repr__(self):
        return f'{self.feedback}'


book_1 = Book('Mark Lutz', 'Learning Python. 5th edition', '2021', 'computer literature')
book_2 = Book('Mark Lutz', 'Learning Python. 5th edition', '2020', 'computer literature')
book_3 = Book('Andrew Park', 'Python Programming for Beginners: The Ultimate Crash Course to Learn Python in 7 Days '
                             'with Step-by-Step Guidance and Hands-On Exercises', '2022', 'computer literature')
book_1.add_feedback("good reading")
book_1.add_feedback("cool book")
book_1.add_feedback("wonderful")
book_3.add_feedback("Recommend")
print(f'book_1: {book_1}\nbook_2: {book_2}\nbook_3: {book_3}')
print(f'\nis book_1 == book_2 - {book_1 == book_2}')
print(f'is book_1 == book_3 - {book_1 == book_3}')
print(f'is book_3 != book_2 - {book_3 != book_2}')
