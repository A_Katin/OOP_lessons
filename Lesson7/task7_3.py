# Напишіть функцію-генератор для отримання n перших простих чисел

def get_list_simple_int(how_many):
    if how_many > 100:
        n = 1000
    else:
        n = 100
    lst = [2]
    for i in range(3, n + 1, 2):
        # делятся на 2 или 5 отбрасываем
        if (i > 10) and (i % 10 == 5):
            continue
        for j in lst:
            if j * j - 1 > i:
                lst.append(i)
                break
            if i % j == 0:
                break
        else:
            lst.append(i)

    for q in lst[int():how_many:]:
        yield q


def main():
    a_count = int(input('Введите желаемое число первых простых чисел: '))
    for i in get_list_simple_int(a_count):
        print(i)


if __name__ == '__main__':
    main()
