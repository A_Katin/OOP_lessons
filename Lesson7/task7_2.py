# Виведіть із списку чисел список квадратів парних чисел. Використовуйте 2 варіанти рішення: генератор та цикл

def main():
    list_int_values = list(map(int, input('Введите числа списка через пробел: ').split()))
    # генератор
    print(f'вар-т через генератор: {[i ** 2 for i in list_int_values if i % 2 == 0]}')

    # цикл
    list_out = []
    for q in list_int_values:
        if q % 2 == 0:
            list_out.append(q ** 2)
    print(f'вариант через цикл: {list_out}')


if __name__ == '__main__':
    main()