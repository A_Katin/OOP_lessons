# Напишіть генератор, який повертає елементи заданого списку у зворотному порядку (аналог reversed).

def gen_1(*list_in):
    if isinstance(list_in[0], list) and len(list_in) == 1:
        list_out = [*list_in[0]]
    else:
        list_out = [*list_in]
    for x in range(len(list_out) - 1, -1, -1):
        yield list_out[x]


def gen_2(list_in):
    for el in list_in[::-1]:
        yield el


def main():
    for i in gen_1(77, 88, 99, 'abc', [*'qwerty']):
        print(i)

    print()

    for i in gen_2([988, 'zz', 'azaz', 'zabcz', 'azaazab', 34, 76, 12, 35, 20, 18, 11, 55, 5]):
        print(i)


if __name__ == '__main__':
    main()