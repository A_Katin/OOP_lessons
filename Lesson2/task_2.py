class GeometricFigure:
    def __init__(self):
        print(f'{self.__class__.__name__} was successfully created\n')


class Rectangle(GeometricFigure):
    pass


class Ellipse(GeometricFigure):
    pass


class Circle(GeometricFigure):
    pass


class Mouse:
    def button_mouse_click(self):
        if type(self) == LeftButton:
            print('the left mouse button was pressed\n')
        elif type(self) == RightButton:
            print('the right mouse button was pressed\n')
        else:
            print('an unknown mouse button was pressed\n')


class RightButtonClick(Mouse):
    pass


class LeftButtonClick(Mouse):
    pass


class LeftButton(Rectangle, LeftButtonClick):
    pass


class RightButton(Rectangle, RightButtonClick):
    pass


fig_1 = Rectangle()
fig_2 = Ellipse()
button_left = LeftButton()
button_right = RightButton()
button_left.button_mouse_click()
button_right.button_mouse_click()

