# Створіть ієрархію класів транспортних засобів. У загальному класі опишіть загальні всім транспортних засобів поля,
# у спадкоємцях – специфічні їм. Створіть кілька екземплярів. Виведіть інформацію щодо кожного транспортного засобу.

class Vehicle:
    def __init__(self, brand, model, year, color, volume, fuel, wheel_drive):
        self.brand = brand
        self.model = model
        self.year = year
        self.color = color
        self.volume = volume
        self.fuel = fuel
        self.wheel_drive = wheel_drive

    def __str__(self):
        return f'Vehicle: {self.brand}, {self.model}, {self.year}, {self.color}, {self.volume}, {self.fuel}, ' \
               f'{self.wheel_drive} wheel drive'

    def __repr__(self):
        return f'Vehicle: {self.brand}, {self.model}, {self.year}, {self.color}, {self.volume}, {self.fuel}, ' \
               f'{self.wheel_drive} wheel drive'


class Trucks(Vehicle):
    type_vehicle = 'Heavy duty truck'

    def __str__(self):
        return f'Vehicle: type {self.type_vehicle}, {self.brand}, {self.model}, {self.year}, {self.color}, ' \
               f'{self.volume}, {self.fuel}, {self.wheel_drive} wheel drive'

    def __repr__(self):
        return f'Vehicle: type {self.type_vehicle}, {self.brand}, {self.model}, {self.year}, {self.color}, ' \
               f'{self.volume}, {self.fuel}, {self.wheel_drive} wheel drive'


class Automobiles(Vehicle):
    type_vehicle = 'Automobile'

    def __str__(self):
        return f'Vehicle: type {self.type_vehicle}, {self.brand}, {self.model}, {self.year}, {self.color}, ' \
               f'{self.volume}, {self.fuel}, {self.wheel_drive} wheel drive'

    def __repr__(self):
        return f'Vehicle: type {self.type_vehicle}, {self.brand}, {self.model}, {self.year}, {self.color}, ' \
               f'{self.volume}, {self.fuel}, {self.wheel_drive} wheel drive'


class Motorcycles(Vehicle):
    type_vehicle = 'Motorcycle'

    def __str__(self):
        return f'Vehicle: type {self.type_vehicle}, {self.brand}, {self.model}, {self.year}, {self.color}, ' \
               f'{self.volume}, {self.fuel}, {self.wheel_drive}-wheel drive'

    def __repr__(self):
        return f'Vehicle: type {self.type_vehicle}, {self.brand}, {self.model}, {self.year}, {self.color}, ' \
               f'{self.volume}, {self.fuel}, {self.wheel_drive}-wheel drive'


class Vans(Vehicle):
    type_vehicle = 'Van'

    def __str__(self):
        return f'Vehicle: type {self.type_vehicle}, {self.brand}, {self.model}, {self.year}, {self.color}, ' \
               f'{self.volume}, {self.fuel}, {self.wheel_drive}-wheel drive'

    def __repr__(self):
        return f'Vehicle: type {self.type_vehicle}, {self.brand}, {self.model}, {self.year}, {self.color}, ' \
               f'{self.volume}, {self.fuel}, {self.wheel_drive}-wheel drive'


truck = Vehicle('PETERBILT', '389', 2022, 'blue', 15.0, 'diesel', 'rear')
bike = Vehicle('HARLEY-DAVIDSON', 'FLHXS', 2021, 'gray', 1.8, 'gas', 'front')
car = Vehicle('FORD', 'EDGE', 2022, 'red', 2.0, 'gas', 'all')
van = Vehicle('FORD', 'TRANSIT CONNECT XL', 2021, 'white', 2.5, 'gas', 'front')

truck_1 = Trucks('FREIGHTLINER', 'CASCADIA 113', 2017, 'red', 12.8, 'diesel', 'rear')
truck_2 = Trucks('VOLVO', 'VN VNL', 2015, 'red', 12.8, 'diesel', 'rear')
truck_3 = Trucks('FORD', 'F650 SUPER DUTY', 2016, 'white', 6.8, 'gas', 'rear')

bike_1 = Motorcycles('YAMAHA', 'WR250 R', 2020, 'black', 1.3, 'gas', 'front')
bike_2 = Motorcycles('HONDA', 'NPS50', 2019, 'blue', 1, 'gas', 'rear')
bike_3 = Motorcycles('BMW', 'S 1000 RR', 2015, 'white', 2.3, 'gas', 'front')

car_1 = Automobiles('HYUNDAI', 'SANTA CRUZ', 2022, 'blue', 2.5, 'gas', 'all')
car_2 = Automobiles('TOYOTA ', '4RUNNER', 2022, 'red', 4.0, 'gas', 'all')
car_3 = Automobiles('ACURA', 'ILX', 2022, 'gray', 2.4, 'gas', 'front')

van_1 = Vans('NISSAN', 'NV200', 2015, 'white', 2.0, 'gas', 'front')
van_2 = Vans('FORD', 'TRANSIT T-250', 2015, 'white', 3.7, 'gas', 'rear')
van_3 = Vans('CHEVROLET', 'EXPRESS G3500 LT', 2017, 'white', 6.0, 'gas', 'rear')

print(truck, truck_1, truck_2, truck_3, sep='\n')
print()
print(bike, bike_1, bike_2, bike_3, sep='\n')
print()
print(car, car_1, car_2, car_3, sep='\n')
print()
print(van, van_1, van_2, van_3, sep='\n')