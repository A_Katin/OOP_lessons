# Cтворіть декоратор @staticmethod для визначення повноліття людини в Україні та Америки
# Cтворіть декоратори @classmethod для формування переліку об'єктів,
# які підрахують кількість повнолітніх людей в Україні та Америці.
from datetime import date


class MyClass1:
    full_age_count_Ukraine = 0
    full_age_count_USA = 0

    def __init__(self, surname, name, age, country):
        self.surname = surname
        self.name = name
        self.age = age
        self.country = country
        if type(self) == MyClass1:
            flg_majority_age = self.validate_majority_age(country, age)
            if flg_majority_age is not None:
                if flg_majority_age:
                    if country == 'Ukraine':
                        self.calculate_full_age_people_by_ukraine()
                    elif country == 'USA':
                        self.calculate_full_age_people_by_usa()

    @classmethod
    def from_birth_year(cls, surname, name, birth_year, country):
        if cls.__name__ != MyClass1.__name__:
            flg_majority_age = cls.validate_majority_age(country, date.today().year - birth_year)
            if flg_majority_age is not None:
                if flg_majority_age:
                    if country == 'Ukraine':
                        cls.calculate_full_age_people_by_ukraine()
                    elif country == 'USA':
                        cls.calculate_full_age_people_by_usa()
        return cls(surname, name, date.today().year - birth_year, country)

    @classmethod
    def calculate_full_age_people_by_usa(cls):
        MyClass1.full_age_count_USA += 1

    @classmethod
    def calculate_full_age_people_by_ukraine(cls):
        MyClass1.full_age_count_Ukraine += 1

    @staticmethod
    def validate_majority_age(country, age):
        dict_majority_age_by_country = {
            'Moldova': 18, 'Sudan': 18, 'Poland': 18, 'Portugal': 18, 'Estonia': 18, 'Germany': 18, 'Belize': 18,
            'USA': 18, 'Tajikistan': 17, 'Taiwan': 20, 'Tanzania': 18, 'Thailand': 20, 'Trinidad and Tobago': 18,
            'Turkey': 21, ' Tunisia': 20, 'Туркменістан': 16, 'Ukraine': 18, 'Hungary': 18, 'Уругвай': 18,
            'Узбекистан': 14, 'Фіджі': 18, 'Філіппіни': 21, 'Finland': 18, 'France': 18, 'Хорватія': 18, 'Чехія': 18,
            'Chili': 18, 'Sweden': 18, 'Switzerland': 18, 'Jamaica': 18, 'Japan': 20
        }
        if country in dict_majority_age_by_country:
            if age >= dict_majority_age_by_country[country]:
                # print(f'Age {age} is the age of majority in the {country}')
                return True
            else:
                # print(f'Age {age} is NOT the age of majority in the {country}')
                return False
        else:
            print('No data for this country')
            return None

    def print_info(self):
        print('\n' + self.name + ' ' + self.surname + "'s age is: " + str(self.age))


class MyClass2(MyClass1):
    color = 'White'


m_per1 = MyClass1('Ivanenko', 'Ivan', 19, 'Ukraine')
m_per1.print_info()

m_per2 = MyClass1.from_birth_year('Dovzhenko', 'Bogdan', 2000, 'Ukraine')
m_per2.print_info()

m_per3 = MyClass2.from_birth_year('Sydorchuk', 'Petro', 2010, 'Ukraine')
m_per3.print_info()

m_per4 = MyClass2.from_birth_year('Makuschenko', 'Dmytro', 2001, 'Ukraine')
m_per4.print_info()

m_per5 = MyClass1('Doe', 'John', 23, 'USA')
m_per5.print_info()

m_per6 = MyClass1.from_birth_year('Manson', 'Charles', 2011, 'USA')
m_per6.print_info()

m_per7 = MyClass2.from_birth_year('Milles', 'Richard', 2002, 'USA')
m_per7.print_info()

m_per8 = MyClass1('Bull', 'Logan', 17, 'USA')
m_per8.print_info()

m_per9 = MyClass2.from_birth_year('Ortan', 'Nicolas', 1974, 'USA')
m_per9.print_info()

m_per10 = MyClass1.from_birth_year('Farbex', 'Kelly', 2003, 'USA')
m_per10.print_info()

m_per11 = MyClass2.from_birth_year('Adams', 'Duglas', 2001, 'USA')
m_per11.print_info()

m_per12 = MyClass2.from_birth_year('Billemy', 'Ben', 1988, 'USA')
m_per12.print_info()

print(f'\nFull age people by Ukraine: {m_per12.full_age_count_Ukraine}')
print(f'\nFull age people by USA: {m_per12.full_age_count_USA}')
