class Editor:
    def view_document(self):
        if type(self) == Editor:
            print('You may only view document')

    def edit_document(self):
        print('Document editing is not available for the free version program!')


class ProEditor(Editor):
    def edit_document(self):
        print('You may edit document')


license_key = '55555-111'
prog_editor = Editor()
user_license_key = input('For further work, enter the license key according to the input mask "11111-111"\n')
if user_license_key == license_key:
    prog_editor = ProEditor()
prog_editor.view_document()
prog_editor.edit_document()
