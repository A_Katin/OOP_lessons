# Створити клас Contact з полями surname, name, age, mob_phone, email. Додати методи get_contact, sent_message.
# Створити клас UpdateContact з полями surname, name, age, mob_phone, email, job. Додати методи get_message.
# Створити екземпляри класів та дослідити стан об'єктів за допомогою атрибутів: __dict__, __base__, __bases__.
# Роздрукувати інформацію на екрані.

class Contact:
    ''' Class Contact '''

    surname = 'Pynja'
    name = 'Ivan'
    age = 27
    mob_phone = '+380639514122'
    email = 'I.Pynja@msn.com'

    def get_contact(self):
        return f'{self.name} {self.surname}, {self.mob_phone}'

    def sent_message(self, text):
        return f'to {self.mob_phone} send message with text {text}'

    def __str__(self):
        return f'({self.name} {self.surname}, {self.age}, {self.mob_phone}, {self.email})'


class UpdateContact(Contact):
    ''' Class UpdateContact '''

    surname, name, age, mob_phone, email, job = 'Konnor', 'Sarah', 34, '+100431155987', 'hope22@umbrella.com', 'Suply'

    def get_message(self, text):
        return f'from {self.mob_phone} get message with text "{text}"'

    def __str__(self):
        return f'({self.name} {self.surname}, {self.age}, {self.mob_phone}, {self.email}, {self.job})'


def condition_class_Contact():
    s = '\nContent of "Contact.__dict__":\n'  # перечень атрибутов класса
    for k, v in Contact.__dict__.items():
        s += f'{k} : {v}\n'
    return s


def main():
    contact_1 = Contact()
    contact_2 = UpdateContact()

    print(condition_class_Contact())
    s = 'Content of "contact_1.__dict__":\n'  # перечень атрибутов экземпляра класса
    for k, v in contact_1.__dict__.items():
        s += f'{k} : {v}\n'
    print(s)

    s = '\nContent of "UpdateContact.__dict__":\n'  # перечень атрибутов класса
    for k, v in UpdateContact.__dict__.items():
        s += f'{k} : {v}\n'
    print(s)
    s = 'Content of "contact_2.__dict__":\n'  # перечень атрибутов экземпляра класса
    for k, v in contact_2.__dict__.items():
        s += f'{k} : {v}\n'
    print(s)

    # по __base__,  инфу не нашел

    print(UpdateContact.__bases__) # родительские classes

    # використати функції hasаttr(), getattr(), setattr(), delattr()

    print(getattr(Contact, 'get_contact')(contact_1))
    print(getattr(contact_1, 'get_contact')())
    print(getattr(UpdateContact, 'get_message')(contact_2, 'Hello, fom Sarah!'))

    def check_class_attr(class_, attribut_):
        if hasattr(class_, attribut_):
            print(f"Class {class_} have attribut {attribut_} = {getattr(class_, attribut_)}")
        else:
            if getattr(class_, attribut_, 'null') == 'null':
                print(f"Class {class_} don't have attribut {attribut_}")

    setattr(Contact, 'job', 'Driver')

    print(condition_class_Contact())
    print()
    check_class_attr(Contact, 'job')
    check_class_attr(UpdateContact, 'job')
    check_class_attr(contact_1, 'job')
    check_class_attr(contact_2, 'job')

    delattr(Contact, 'job')
    print(condition_class_Contact())
    print()
    check_class_attr(Contact, 'job')
    check_class_attr(UpdateContact, 'job')
    check_class_attr(contact_1, 'job')
    check_class_attr(contact_2, 'job')
    print()
    print('contact_1 is class Contact - {}'.format(isinstance(contact_1, Contact)))
    print('contact_2 is class UpdateContact - {}'.format(isinstance(contact_2, UpdateContact)))
    print(f'UpdateContact is subclass Contact - {issubclass(UpdateContact, Contact)}')
    print()

    # надрукуйте у терміналі всі методи, які містяться у класі Contact
    dir(Contact)


if __name__ == '__main__':
    main()
