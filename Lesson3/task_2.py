# Створіть 2 класи мови, наприклад, англійська та іспанська. В обох класів має бути метод greeting().
# Обидва створюють різні привітання. Створіть два відповідні об'єкти з двох класів вище та викличте дії
# цих двох об'єктів в одній функції (функція hello_friend).

class English:
    @staticmethod
    def greeting():
        print('\nHello, dear friend!')


class Spanish:
    @staticmethod
    def greeting():
        print('\nHola querido amigo!')


def hello_friend():
    num_todo = ''
    eng_cl = English()
    span_cl = Spanish()
    while num_todo != '3':
        print(f'\n          Please select language"', '-' * 70,
              '1. Spanish\n'
              '2. English\n'
              '3. Exit',
              sep='\n')
        num_todo = input('\nmake your choice: ')
        match num_todo:
            case '1':
                span_cl.greeting()
            case '2':
                eng_cl.greeting()
            case '3':
                break
            case _:
                print('input incorrect data')
                break


if __name__ == '__main__':
    hello_friend()
