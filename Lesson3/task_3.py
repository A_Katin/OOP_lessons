# Створіть клас, що описує температуру і дозволяє задавати та отримувати температуру за шкалою Цельсія та Фаренгейта,
# причому дані можуть бути задані в одній шкалі, а отримані в іншій.

class Temperature:
    def __init__(self, temp_value, temp_simbol: str):
        if temp_simbol == 'F':
            self.in_fahrenheit = temp_value
        elif temp_simbol == 'C':
            self.in_celsius = temp_value
        else:
            raise TypeError('incorrect data')

    def __str__(self):
        return f'\n {self.__in_celsius}°C / {self.__in_fahrenheit}°F'

    @staticmethod
    def celsium_to_fahrenheit(val_in_celsius):
        return round(9 / 5 * val_in_celsius + 32, 2)

    @staticmethod
    def fahrenheit_to_celsium(val_in_fahrenheit):
        return round(5 / 9 * (val_in_fahrenheit - 32), 2)

    @property
    def in_celsius(self):
        return self.__in_celsius

    @in_celsius.setter
    def in_celsius(self, value_in_celsius):
        if isinstance(value_in_celsius, str):
            value_in_celsius = float(value_in_celsius)
        if isinstance(value_in_celsius, int):
            value_in_celsius = float(value_in_celsius)
        if not isinstance(value_in_celsius, float):
            raise TypeError('\nTemperature value must be float type')
        if value_in_celsius < -273.15:
            raise TypeError('\nTemperature value must be greater than absolute zero -273.15°C')
        self.__in_celsius = value_in_celsius
        self.__in_fahrenheit = self.celsium_to_fahrenheit(value_in_celsius)

    @property
    def in_fahrenheit(self):
        return self.__in_fahrenheit

    @in_fahrenheit.setter
    def in_fahrenheit(self, value_in_fahrenheit):
        if isinstance(value_in_fahrenheit, str):
            value_in_fahrenheit = float(value_in_fahrenheit)
        if isinstance(value_in_fahrenheit, int):
            value_in_fahrenheit = float(value_in_fahrenheit)
        if not isinstance(value_in_fahrenheit, float):
            raise TypeError('\nTemperature value must be float type')
        if value_in_fahrenheit < -459.67:
            raise TypeError('\nTemperature value must be greater than absolute zero -459.67°F')
        self.__in_fahrenheit = value_in_fahrenheit
        self.__in_celsius = self.fahrenheit_to_celsium(value_in_fahrenheit)


def temperature_converter():
    num_todo = ''
    while num_todo != '3':
        print(f'\n          Temperature converter', '-' * 50,
              '1. °F to °C\n'
              '2. °C to °F\n'
              '3. Exit',
              sep='\n')
        num_todo = input('\nmake your choice: ')
        match num_todo:
            case '1':
                convert_value = float(input('input value on °F for convert: '))
                try:
                    converter = Temperature(convert_value, 'F')
                    print(converter)
                except TypeError as msg:
                    print(msg)
            case '2':
                convert_value = float(input('input value on °F for convert: '))
                try:
                    converter = Temperature(convert_value, 'C')
                    print(converter)
                except TypeError as msg:
                    print(msg)
            case '3':
                break
            case _:
                print('input incorrect data')
                break


if __name__ == '__main__':
    temperature_converter()
