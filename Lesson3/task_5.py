# створіть додаткові класи-нащадки Cone та Paraboloid від класу Shape. Перевизначте їх методи.
# Створіть екземпляри відповідних класів за скористайтеся всіма методами.
# В результаті повинно з’явитися зображення. Перегляньте їх.
from PIL import Image, ImageDraw


class Shape:
    def __init__(self):
        # Колір тла
        self.back_color = (155, 213, 117, 100)
        # Створюємо зображення 500 * 500
        self.im = Image.new('RGBA', (500, 500), self.back_color)
        self.draw1 = ImageDraw.Draw(self.im)

    def draw(self):
        pass

    def erase(self):
        self.im = Image.new('RGBA', (500, 500), self.back_color)
        self.draw1 = ImageDraw.Draw(self.im)

    def save(self):
        print("Background was created")
        return self.im.save('picture.png', quality=95)


class Circle(Shape):
    def draw(self):
        self.draw1.ellipse((75, 100, 175, 200), fill='yellow', outline=(255, 255, 255))

    def erase(self):
        self.draw1.ellipse((75, 100, 175, 200), fill=self.back_color)

    def save(self):
        print("Image with circle was created")
        return self.im.save('draw-circle.png', quality=95)


class Square(Shape):
    def draw(self):
        self.draw1.rectangle((200, 100, 300, 200), fill='blue', outline=(255, 255, 255))

    def erase(self):
        self.draw1.rectangle((200, 200, 300, 200), fill=self.back_color, outline=(255, 255, 255))

    def save(self):
        print("Image with square was created")
        return self.im.save('draw-square.png', quality=95)


class Triangle(Shape):
    def draw(self):
        self.draw1.polygon([(400, 100), (350, 200), (450, 200)], fill=(255, 0, 0))

    def erase(self):
        self.draw1.polygon([(400, 100), (350, 200), (450, 200)], fill=self.back_color)

    def save(self):
        print("Image with triangle was created")
        return self.im.save('draw-triangle.png', quality=95)


class Cone(Shape):
    def draw(self):
        self.draw1.polygon([(30, 440), (250, 200), (470, 440)], fill='yellow')
        self.draw1.pieslice([(29, 395), (471, 487)], 0, 180, fill='yellow')

    def erase(self):
        super().erase()

    def save(self):
        print("Image with cone was created")
        return self.im.save('draw-cone.png', quality=95)

    def show(self):
        self.im.show()


class Paraboloid(Shape):
    def draw(self):
        x = 0
        x0 = 75
        y0 = 100
        x1 = 425
        y1 = 150
        while x <= 145:
            self.draw1.ellipse((x0 + x, y0 + x, x1 - x, y1 + x), fill='blue')
            x += 5
        self.draw1.ellipse((x0, y0, x1, y1), fill='blue', outline=(255, 255, 255))

    def erase(self):
        super().erase()

    def save(self):
        print("Image with paraboloid was created")
        return self.im.save('draw-paraboloid.png', quality=95)

    def show(self):
        self.im.show()


parabol = Paraboloid()
parabol.draw()
parabol.save()
parabol.show()
cone = Cone()
cone.draw()
cone.save()
cone.show()


# s = Shape()
# s.save()
# c = Circle()
# c.draw()
# c.erase()
# c.save()
# sq = Square()
# sq.draw()
# sq.erase()
# sq.save()
# t = Triangle()
# t.draw()
# t.erase()
# t.save()
