# Опишіть два класи Base та його спадкоємця Child з методами method(),
# який виводить на консоль фрази відповідно "Hello from Base" та "Hello from Child".
class Base:
    '''This is class Base'''
    def method(self):
        print('Hello from Base')


class Child(Base):
    '''This is class Child'''
    def method(self):
        print('Hello from Child')
        super().method()


if __name__ == '__main__':
    y = Child()
    print(f'\n{Child.__doc__}')
    y.method()
