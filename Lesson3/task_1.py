# Створіть клас, який описує автомобіль. Які атрибути та методи мають бути повністю інкапсульовані?
# Доступ до таких атрибутів та зміну даних реалізуйте через спеціальні методи (get, set).
from string import digits


class Car:
    __set_of_vin_codes = set()

    def __init__(self, brand, model, color='', volume='', **kwargs):
        self.__vin_code = None
        self.brand = brand
        self.model = model
        self.color = color
        self.volume = volume
        self.vin_code = kwargs.get("VIN", None)

    @property
    def set_of_vin_codes(self):
        raise AttributeError('\nThe attribute "set_of_vin_codes" is not available for reading')

    @set_of_vin_codes.setter
    def set_of_vin_codes(self, code):
        raise AttributeError('\nThe attribute "set_of_vin_codes" is not writable')

    @staticmethod
    def is_include_number(code):
        for digit in digits:
            if digit in code:
                return True
        return False

    @property
    def vin_code(self):
        return self.__vin_code

    @vin_code.setter
    def vin_code(self, code):
        if code is not None:
            if self.__vin_code is None:
                if not isinstance(code, str):
                    raise TypeError('\nVIN-code must be string type')
                if len(code) < 17:
                    raise TypeError('\nVIN-code must be 17 characters long')
                if not self.is_include_number(code):
                    raise TypeError('\nVIN-must include digits')
                if code not in self.__set_of_vin_codes:
                    self.__set_of_vin_codes.add(code)
                    self.__vin_code = code
                else:
                    print("\nThis VIN-code is already use, input another code")
            else:
                print("Car's VIN-code could not to be changing\n")

    def __str__(self):
        return f'{self.brand}, {self.model}' \
               f'{", "+self.color if self.color else ""}' \
               f'{", "+self.volume if self.volume else ""}' \
               f'{", "+self.vin_code if self.vin_code else ""}'


if __name__ == '__main__':
    car_1 = Car('Audi', 'Q3', '', '2.0', VIN='WA1DECF37M1598201')
    print(car_1)
    car_1.color = 'black'
    print(car_1)

    car_2 = Car('KIA', 'Forte', 'red', '1.6')
    print(car_2)
    car_2 = Car('KIA', 'Forte', 'red', '1.6', VIN='3KPF54AD0ME67ERT3')
    print(car_2)

    car_3 = Car('Toyota', 'Prius', 'white', '41.3Kw', VIN='JTDKAMFP6M3HOBJO7')
    print(car_3)
    car_3.vin_code = 'qwerty'

    car_4 = Car('Acura', 'RDX', 'silver', '3.6')
    print(car_4)
    try:
        car_4.vin_code = 58821136647785  # Error. VIN like int
    except TypeError as msg:
        print(msg)

    try:
        car_4.vin_code = 'WAYDIDYTYTD'  # Error. VIN < 17 simbols
    except TypeError as msg:
        print(msg)

    try:
        car_4.vin_code = 'WAYDIDYTYTDGHVBTP'  # Error. VIN string without digits
    except TypeError as msg:
        print(msg)

    car_4.vin_code = 'WA1DECF37M1598201'
    car_4.vin_code = '5J8TC2H82NL006061'
    print(car_4)

    try:
        print(car_4.set_of_vin_codes)  # Error. Reading set of VIN-codes
    except AttributeError as msg:
        print(msg)
