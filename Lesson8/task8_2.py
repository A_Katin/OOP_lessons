# Створіть програму, яка емулює роботу сервісу зі скорочення посилань. Повинна бути реалізована можливість
# введення початкового посилання та короткої назви і отримання початкового посилання за її назвою.
# Модифікуйте вихідний код сервісу зі скорочення посилань так,
# щоб він зберігав базу посилань на диску і не «забув» при перезапуску.
# сделал в виде словаря новостных изданий {сокращ. название: URL интернет-ресурса} при помощи модуля pickle
import pickle
import os
from sys import platform


def get_dict_default():
    dic_urls = {
        'bbc': 'www.bbc.com/news/world', 'tnyt': 'www.nytimes.com', 'bfnews': 'buzzfeednews.com/section/world',
        'aljazeera': 'aljazeera.com', 'db': 'defense-blog.com', 'gi': 'globalissues.org',
        'tcb': 'thecipherbrief.com', 'cnn': 'edition.cnn.com/world', 'wp': 'washingtonpost.com/world',
        'cnbc': 'cnbc.com/world-top-news', 'reuters': 'reuters.com', 'dw': 'www.dw.com', 'cbc': 'cbc.ca/news',
        'time': 'time.com/', 'unian': 'www.unian.ua/', 'unn': 'www.unn.com.ua/'}

    return dic_urls


def main():
    num_todo = int()
    file_name = ''
    dict_in = {}
    # корректируем имя файла в зависимости от платформы
    if platform == "linux" or platform == "linux2":
        file_name = r'_files\file_db'
    elif platform == "darwin":
        file_name = r'_files\file_db'
    elif platform == "win32":
        file_name = r'_files/file_db'
    # проверяем существование файла и беспроблемного доступа к нему
    # отсюда получаем словарь
    if os.path.exists(file_name) and os.access(file_name, os.R_OK):
        with open(file_name, 'rb') as f_in:
            dict_in = pickle.load(f_in)
    if len(dict_in) == 0:
        dict_in = get_dict_default()

    while num_todo != 4:
        print('\n{:*^60}'.format('*') + '\n{:*^60}'.format(' Database of online media news ') +
              '\n{:|^60}'.format('|') +
              '\n{:|^60}\n'.format(f' Now database contain {len(dict_in)} records ') +
              '\n{:30}'.format(f'1. All records') +
              '\n{:30}'.format(f'2. Add record') +
              '\n{:30}'.format(f'3. More about record')
              )
        print('\n{:30}'.format(f'4. Exit'))
        choice = int(input('\nSelect next action: '))
        match choice:
            case 1:
                print('\n+' + '{:+^61}'.format('+') + '+')
                for word, link_ in sorted(dict_in.items()):
                    slash_on_end = '' if link_[-1::] == '/' else '/'
                    print('|' + f"{word.upper() if len(word) <= 4 else word.capitalize():^15}  |    https://{link_}"
                                f"{slash_on_end} ")
                print()
            case 2:
                new_abc = input('Add short name: ').lower()
                new_url = input('Link: ').strip()
                new_url = new_url + '' if new_url[-1::] == '/' else '/'
                dict_in[new_abc] = new_url
            case 3:
                find_abc = input('Find short name: ').lower()
                print(f'\nOnline news resource: {find_abc.upper() if len(find_abc) <= 4 else find_abc.capitalize():<16} '
                      f'Link: https://{dict_in[find_abc]}') if dict_in.get(find_abc) else print(
                    '\ndata not found')
            case 4:
                with open(file_name, 'wb') as f_out:
                    pickle.dump(dict_in, f_out)
                exit()
            case _:
                print('incorrect data')
                with open(file_name, 'wb') as f_out:
                    pickle.dump(dict_in, f_out)
                exit()


if __name__ == '__main__':
    main()
