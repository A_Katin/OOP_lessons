# Напишіть скрипт, який створює текстовий файл і записує до нього 10000 випадкових дійсних чисел.
# Створіть ще один скрипт, який читає числа з файлу та виводить на екран їхню суму.
import random


def main():
    rand_values = ''
    rand_count = 10000
    i = 1
    while i <= rand_count:
        rand_values += str(random.random()) + ("|" if i < rand_count else '')
        i += 1

    with open('random_values.txt', 'w') as text_file:
        text_file.write(rand_values)

    with open('random_values.txt', 'r') as text_file:
        t_str_lst = list(map(float, text_file.readline().split('|')))
        print(sum(t_str_lst))


if __name__ == '__main__':
    main()
