# Створіть список товарів в інтернет-магазині. Серіалізуйте його за допомогою pickle та збережіть у JSON.
import pickle
import csv
import os
from sys import platform
from datetime import datetime


def first_init() -> list:
    main_list = [
        # (main_items_group, items_group, item, vendor_code, manufacture, price, quantity)
        ('алфавитные книги', '', 'Книга алфавитная 100х190мм, 80 листов', '210 05', 'ПОЛИГРАФИСТ', '105', '480',),
        ('алфавитные книги', '', 'Книга алфавитная 135х285мм, 176 листов', '213 05', 'ПОЛИГРАФИСТ', '275', '500',),
        ('алфавитные книги', '', 'Книга алфавитная 145х202мм, 112 листов', '211 05', 'ПОЛИГРАФИСТ', '192', '500',),
        ('бумага для заметок', 'бумага для записей белая', 'Блок белой бумаги для записей 152х102мм, 170 листов',
         'BE.7159', 'BUROMAX', '34.60', '10000',),
        ('бумага для заметок', 'бумага для записей белая', 'Блок белой бумаги для записей 90х90х30мм не склеенный',
         'BM.2209', 'BUROMAX', '45.90', '11000',),
        ('бумага для заметок', 'бумага для записей белая', 'Блок белой бумаги для записей Elit White 90х90х40мм '
                                                           'не склеенный', '8006АХ', 'AXENT', '76.89', '25000',),
        ('бумага для заметок', 'бумага для записей белая', 'Блок белой бумаги для записей Elit White 90х90х70мм '
                                                           'не склеенный', '8008АХ', 'AXENT', '126.72', '15000',),
        ('бумага для заметок', 'стикеры', 'Блок бумаги для заметок 38*51мм 4х50л НЕОН', '7578001PL', 'DONAU', '87.28',
         '100',),
        ('бумага для заметок', 'стикеры', 'Блок бумаги для заметок 50*50мм 250л', '7575001PL', 'DONAU', '118.25',
         '11230',),
        ('бумага для заметок', 'стикеры', 'Блок бумаги для заметок 38*51мм*3шт по 100л.', 'BM.2319-99', 'BUROMAX',
         '25.51',
         '17423',),
        ('бумага для заметок', 'стикеры', 'Блок бумаги для заметок 76*76мм НЕОН ГИРЛЯНДА, 100л.', 'BM.2323-97',
         'BUROMAX',
         '40.30', '6000',),
        ('бумага для заметок', 'стикеры', 'Блок бумаги для заметок 76*76мм 400л', '7574001PL', 'DONAU', '315.28',
         '5000',),
        ('бумага для заметок', 'бумага для записей цветная', 'Блок цветной бумаги для записей РАДУГА 80х80х30мм '
                                                             'склеенный', 'BM.2232', 'BUROMAX', '32.90', '55000',),
        ('бумага для заметок', 'бумага для записей цветная', 'Блок цветной бумаги для записей Elit Color 90х90х70мм '
                                                             'не склеенный', '8028АХ', 'AXENT', '190.08', '500',),
        ('блокноты и тетради', 'блокноты формат А4', 'Блокнот на пружине сверху METROPOLIS формат А4, 48л., красный',
         '144.BM.24445101-05', 'BUROMAX', '47.75', '1000',),
        ('блокноты и тетради', 'блокноты формат А4', 'Блокнот на спирали формат А4, 48л.', '152.48-194', 'РЕВЕРС',
         '48.54',
         '2000',),
        ('блокноты и тетради', 'блокноты формат А4', 'Тетрадь канцелярская формат А4, 96л.', 'BM.2428', 'BUROMAX',
         '102.15',
         '9000',),
        ('блокноты и тетради', 'блокноты формат А5', 'Блокнот SIGHT формат А5, 80л.', 'BM.24582105-13', 'BUROMAX',
         '85.19',
         '10000',),
        ('блокноты и тетради', 'блокноты формат А5', 'Блокнот на пружине сбоку SENSE, клетка, формат А5, 80л, мятный',
         'BM.24572101-38', 'BUROMAX ', '99.58', '1200',),
        ('блокноты и тетради', 'блокноты формат А5', 'Блокнот на пружине сверху METROPOLIS формат А5, 48л., салатовый',
         'BM.24545101-15', 'BUROMAX', '27.48', '5500',),
        ('блокноты и тетради', 'блокноты формат А5', 'Блокнот на спирали формат А5, 48л.', '48-168', 'РЕВЕРС', '28.44',
         '60',),
        ('блокноты и тетради', 'блокноты формат А6', 'Блокнот в твердой обложке FLORA А6 64 листа в клетку белый',
         'BM.24614102-12', 'BUROMAX', '49.07', '4000',),
        ('блокноты и тетради', 'блокноты формат А6', 'Блокнот на спирали формат А6, 48л.', '48-169', 'РЕВЕРС', '15.66',
         '10000',),
        (
            'блокноты и тетради', 'блокноты формат А6', 'Блокнот на спирали формат А6, 96л.', 'BM.2422', 'BUROMAX',
            '48.93',
            '9000',),
        ('блокноты и тетради', 'блокноты формат А6', 'Записная книга на пружине формат А6, 192л., клетка', '192-199',
         'РЕВЕРС', '59.32', '5000',),
        (
            'блокноты и тетради', 'блокноты формат А7', 'Блокнот на пружине сверху формат А7, 48л., бордовый',
            'BM.2490-13',
            'BUROMAX', '10.48', '2000',),
        ('блокноты и тетради', 'блокноты формат А7', 'Блокнот на пружине сверху формат А7, 48л., синий', 'BM.2490-02',
         'BUROMAX', '10.48', '2000',),
        ('блокноты и тетради', 'блокноты формат А7', 'Блокнот на спирали формат А7, 48л', '48-152', 'РЕВЕРС', '10.53',
         '100',),
        ('блокноты и тетради', 'блокноты формат А7', 'Записная книга на пружине формат А7, 96л., клетка', '	96-213',
         'РЕВЕРС', '17.33', '100',),
        (
            'блокноты и тетради', 'книги канцелярские', 'Книга канцелярская А4 192 листа в клетку Axent Dubai',
            '8423-23АХ',
            'AXENT', '251', '1000',),
        ('блокноты и тетради', 'книги канцелярские', 'Книга канцелярская А4 144 листа в клетку MODEST',
         'BM.24419102-03 ',
         'BUROMAX', '175.38', '2000',),
        ('блокноты и тетради', 'книги канцелярские', 'Книга канцелярская А4 96 листов СЕЗОНЫ', 'BM.2408', 'BUROMAX',
         '121.53', '3000',),
        ('блокноты и тетради', 'книги канцелярские',
         'Книга канцелярская А4 96 листов в клетку FAVOURITE PASTEL сиреневая',
         'BM.2400-426', 'BUROMAX', '161.85', '3000',),
        ('конверты', 'стандартные конверты', 'Конверт DL(Е65) скл белый', '2052 ', 'KUVERT', '3.50', '10000',),
        ('конверты', 'стандартные конверты', 'Конверт В4 скл коричневый', '5240 ', 'KUVERT', '10', '10000',),
        ('конверты', 'стандартные конверты', 'Конверт С4 скл белый', '4040 ', 'KUVERT', '8', '10000',),
        ('конверты', 'стандартные конверты', 'Конверт С5 скл белый', '3444', 'KUVERT', '4.50', '10000',),
        ('конверты', 'противоударные конверты', 'Конверт противоударный AIRPOK №11', '9111', 'KUVERT', '3.47', '5000',),
        ('конверты', 'противоударные конверты', 'Конверт противоударный AIRPOK №12', '9116', 'KUVERT', '4.55', '5000',),
        ('конверты', 'противоударные конверты', 'Конверт противоударный AIRPOK №13', '9121', 'KUVERT', '5.55', '5000',),
        ('конверты', 'противоударные конверты', 'Конверт противоударный AIRPOK №14', '9126', 'KUVERT', '7.25', '5000',),
        ('', '', 'Калькулятор 8 розрядів 100*124*33мм', 'BS-208', 'Brilliant ', '220', '500',),
        ('', '', 'Калькулятор 14 розрядів 150*200*25мм', 'SDC-554S', 'Citizen', '540', '500',),
        ('', '', 'Калькулятор 12 розрядів бухгалтерський 203*158*30,5мм', 'O75575', 'Optima ', '465', '500',),
    ]
    return main_list


def get_list_main_groups(main_list, n: int = 0, ) -> list:
    '''
    receive list from [n] elements row of main_list
    :return: list from [n] elements row main_list
    '''

    return list(set([x[n] for x in main_list if x[n]]))


def add_new_item_to_main_list(main_list) -> None:
    '''
    collect information data from user about new item and add this data to the start of main_list
    :return: None
    '''
    i_ = 1
    new_item_main_group = ''
    new_item_subgroup = ''
    main_groups_lst = get_list_main_groups(main_list)
    main_groups_lst.append('товары без группы')
    main_groups_lst_ = sorted(main_groups_lst)
    print()

    while i_ <= len(main_groups_lst_):
        print(f'{i_}. {main_groups_lst_[i_ - 1].capitalize()}')
        i_ += 1
    num_main_group_n = int(input('\nВыберите номер группы нового товара: '))
    if num_main_group_n != main_groups_lst_.index('товары без группы') + 1:
        new_item_main_group = main_groups_lst_[num_main_group_n - 1]
        print(f'\n{new_item_main_group.capitalize()}')
        subgroups_lst_ = list(set([y[1] for y in main_list if y[0] == new_item_main_group]))
        subgroups_lst_.append('без группы')
        print()
        y = 1
        while y <= len(subgroups_lst_):
            if subgroups_lst_[y - 1]:
                print(f'{y}. {subgroups_lst_[y - 1].capitalize()}')
            y += 1
        num_subgroup_n = int(input('\nВыберите номер подгруппы нового товара: '))
        if num_subgroup_n != subgroups_lst_.index('без группы') + 1:
            new_item_subgroup = subgroups_lst_[num_subgroup_n - 1]
    new_item_name = input('\nВведите название нового товара: ')
    new_item_vendor_code = input('\nВведите артикул нового товара: ')
    new_item_manufacture = input('\nВведите производителя нового товара: ')
    new_item_price = float(input('\nВведите цену нового товара: '))
    new_item_quantity = int(input('\nВведите количество нового товара: '))
    main_list.insert(0, (new_item_main_group, new_item_subgroup, new_item_name,
                         new_item_vendor_code, new_item_manufacture, new_item_price, new_item_quantity,))


def main():
    num_ex = ''
    file_name = ''
    price_file_name = ''
    items_list = []
    today_str = datetime.now().strftime('%d_%m_%Y')
    # корректируем имя файла в зависимости от платформы
    if platform == "linux" or platform == "linux2":
        file_name = r'_files\items.bin'
    elif platform == "darwin":
        file_name = r'_files\items.bin'
    elif platform == "win32":
        file_name = r'_files/items.bin'
    # проверяем существование файла и беспроблемного доступа к нему
    # отсюда получаем словарь
    if os.path.exists(file_name) and os.access(file_name, os.R_OK):
        with open(file_name, 'rb') as f_in:
            items_list = pickle.load(f_in)
    if len(items_list) == 0:
        items_list = first_init()

    while num_ex != '7':
        print(f'\n          Прототип программы "Магазин канцтоваров"', '-' * 70,
              '1. Поиск товара\t\t\t3. Вывести каталог товаров\n'
              '2. Добавить запись\t\t4. Вывести товары определенной группы\n'
              '                   \t\t5. Выгрузить прайс-лист в формате JSON',
              '                   \t\t6. Выгрузить прайс-лист в формате CSV',
              '                   \t\t7. Выход',
              sep='\n')
        num_ex = input('\nВыберите действие: ')
        match num_ex:
            case '1':
                find_name_item = input('Введите название товара: ').strip().lower()
                list_finded_items = []
                for str_item in items_list:
                    item = str_item[2].lower()
                    if item.find(find_name_item) != -1:
                        list_finded_items.append(items_list.index(str_item))
                if len(list_finded_items) > 0:
                    print()
                    for str_index_item in list_finded_items:
                        print('%-50s %s%-10s %s%d' % (items_list[str_index_item][2], 'Цена: ',
                                                      items_list[str_index_item][5] + ' грн.', 'Остаток: ',
                                                      int(items_list[str_index_item][6])))
                else:
                    print('\n... Указанный товар не найден.')
            case '2':
                add_new_item_to_main_list(items_list)
            case '3':
                main_groups_lst = get_list_main_groups(items_list)
                for group_ in sorted(main_groups_lst):
                    print(f'\n{group_.capitalize()}')
                    subgroups_lst = list(set([y[1] for y in items_list if y[0] == group_]))
                    for subgroup_ in sorted(subgroups_lst):
                        if subgroup_:
                            print(f'  {subgroup_.capitalize()}')
                        for str_item in items_list:
                            if str_item[0] == group_ and str_item[1] == subgroup_:
                                print(f'    {str_item[2].strip()} | {str_item[3].strip()}'
                                      f' | {str_item[4].strip()} | {float(str_item[5]):.2f} грн. | {str_item[6]}')
                # элементы без группы
                for str_item in items_list:
                    if not str_item[0] and not str_item[1]:
                        print(f'{str_item[2].strip()} | {str_item[3].strip()}'
                              f' | {str_item[4].strip()} | {float(str_item[5]):.2f} грн. | {str_item[6]}')
            case '4':
                main_groups_lst = get_list_main_groups(items_list)
                main_groups_lst.append('товары без группы')
                main_groups_lst = sorted(main_groups_lst)
                print()
                i = 1
                while i <= len(main_groups_lst):
                    print(f'{i}. {main_groups_lst[i - 1].capitalize()}')
                    i += 1
                num_main_group = int(input('\nВыберите номер группы товара: '))
                if num_main_group == main_groups_lst.index('товары без группы') + 1:
                    print('\nТовары без группы')
                    for str_item in items_list:
                        if not str_item[0] and not str_item[1]:
                            print(f'{str_item[2].strip()} | {str_item[3].strip()}'
                                  f' | {str_item[4].strip()} | {float(str_item[5]):.2f} грн. | {str_item[6]}')
                else:
                    group_ = main_groups_lst[num_main_group - 1]
                    print(f'\n{group_.capitalize()}')
                    subgroups_lst = list(set([y[1] for y in items_list if y[0] == group_]))
                    for subgroup_ in sorted(subgroups_lst):
                        if subgroup_:
                            print(f'  {subgroup_.capitalize()}')
                        for str_item in items_list:
                            if str_item[0] == group_ and str_item[1] == subgroup_:
                                print(f'    {str_item[2].strip()} | {str_item[3].strip()}'
                                      f' | {str_item[4].strip()} | {float(str_item[5]):.2f} грн. | {str_item[6]}')
            case '5':
                if platform == "linux" or platform == "linux2":
                    price_file_name = r'_files\pricelist_'+f'{today_str}'+'.json'
                elif platform == "darwin":
                    price_file_name = r'_files\pricelist_'+f'{today_str}'+'.json'
                elif platform == "win32":
                    price_file_name = r'_files/pricelist_'+f'{today_str}'+'.json'
                if len(items_list) == 0:
                    items_list = first_init()
                with open(price_file_name, 'wb') as f_out:
                    pickle.dump(items_list, f_out)
                if os.path.exists(price_file_name) and os.access(price_file_name, os.R_OK):
                    with open(price_file_name, 'rb') as f_in:
                        print(f_in.read())
                    print(f'Прайс-лист {price_file_name} в формате JSON успешно выгружен!')
            case '6':
                if platform == "linux" or platform == "linux2":
                    price_file_name = r'_files\pricelist_' + f'{today_str}' + '.csv'
                elif platform == "darwin":
                    price_file_name = r'_files\pricelist_' + f'{today_str}' + '.csv'
                elif platform == "win32":
                    price_file_name = r'_files/pricelist_' + f'{today_str}' + '.csv'
                if len(items_list) == 0:
                    items_list = first_init()
                with open(price_file_name, 'w') as f_out:
                    csv.writer(f_out).writerows(items_list)
                if os.path.exists(price_file_name) and os.access(price_file_name, os.R_OK):
                    with open(price_file_name, 'r') as f_in:
                        print(f_in.read())
                    print(f'Прайс-лист {price_file_name} в формате CSV успешно выгружен!')
            case '7':
                with open(file_name, 'wb') as f_out:
                    pickle.dump(items_list, f_out)
                break
            case _:
                with open(file_name, 'wb') as f_out:
                    pickle.dump(items_list, f_out)
                print('введены некорректные данные')
                break


if __name__ == '__main__':
    main()
