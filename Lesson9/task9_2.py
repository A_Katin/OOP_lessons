# Повторіть інформацію про розглянуті на уроці стандартні модулі.
# Ознайомтеся також із модулями calendar, heapq, bisect, array, enum.
from math import pi, e
import sys
import calendar
import random
import heapq
import bisect
import enum
from array import *


def main():
    print(f' атрибут "pi" из модуля math {pi}')
    print(f' атрибут "e" из модуля math {e}')
    print(f'\n[sys.path]:', *sys.path, sep='\n')

    print(f'\n{calendar.LocaleTextCalendar(locale="Ukrainian_Ukraine").pryear(2023)}\n')

    # работа с модулем heapq
    print('{:_^50}'.format(' Работа с модулем heapq '))
    mylist = [i for i in range(1, 10)]
    random.shuffle(mylist)
    print(f'начальный список {mylist}')
    # Создание кучи
    heapq.heapify(mylist)
    print(f'Создание кучи {mylist}')
    # Вставка элементов данных в кучу
    heapq.heappush(mylist, 20)
    print(f'Вставка элемента данных в кучу {mylist}')
    # Удаление элемента данных из кучи
    heapq.heappop(mylist)
    print(f'Удаление элемента данных из кучи {mylist}')
    # Замена элемента данных в куче
    heapq.heapreplace(mylist, 99)
    print(f'Замена элемента данных в куче {mylist}')
    print('{:_^50}'.format('_'))

    print('\n{:_^50}'.format(' Работа с модулем bisect '))
    mylist.sort()
    print(f'отсортированный список {mylist}')
    print(f'индекс, по которому нужно вставить 19 в mylist, сохраняя список отсортированным - '
          f'{bisect.bisect(mylist, 19)}')
    print(f'элемент 9, который вы хотите вставить, уже присутствует в списке, получаем индекс перед этим элементом - '
          f'{bisect.bisect_left(mylist, 9)}')
    print(f'элемент 9, который вы хотите вставить, уже присутствует в списке, получаем индекс после этого элемента - '
          f'{bisect.bisect_right(mylist, 9)}')
    print(f'вставка элемента 19 в список, не нарушая его порядок')
    bisect.insort(mylist, 19)
    print(f'отсортированный список {mylist}')
    print(f'вставка элемента 19 слева от существующего в списке, не нарушая его порядок')
    bisect.insort_left(mylist, 19)
    print(f'отсортированный список {mylist}')
    print('{:_^50}'.format('_'))

    print('\n{:_^50}'.format(' Работа с модулем array '))
    '''
      Код типа	Тип в python	Минимальный размер в байтах
        'b'	     int	                1
        'B'	     int	                1
        'h'	     int	                2
        'H'	     int	                2
        'i'	     int	                2
        'I'	     int	                2
        'l'	     int	                4
        'L'      int	                4
        'q'	     int	                8
        'Q'	     int	                8
        'f'	     float	                4
        'd'	     float	                8
    '''
    my_array = array('i', [*mylist])
    print(f'массив {my_array}')
    my_array.append(6)
    print('Добавить значение 6 в массив с помощью метода append() ', my_array)
    my_array.insert(0, 0)
    print('Вставить значение 0 в массив с помощью метода insert() ', my_array)
    my_extnd_array = array('i', [37, 88, 69, 100])
    my_array.extend(my_extnd_array)
    print('Расширение на более чем одно значение с помощью extend()', my_array)
    my_array = array('i', [1, 2, 3, 4, 5])
    print(f'массив {my_array}')
    c = [11, 12, 13, 5, 1, 11, 12]
    my_array.fromlist(c)
    print('Добавить элементы из списка [11, 12, 13] в массив, используя метод fromlist()', my_array)
    print(f'массив {my_array}')
    my_array.remove(12)
    print(f'Удалить элемент массива, используя метод remove() {my_array}')
    print(f'массив {my_array}')
    my_array.pop()
    print(f'Удалить последний элемент массива методом pop() {my_array}')
    print(f'массив {my_array}')
    my_array.reverse()
    print(f'Обратный массив, используя метод reverse() {my_array}')
    print('Получить информацию о буфере массива с помощью метода buffer_info(). Этот метод предоставляет '
          'начальный адрес буфера массива в памяти и количество элементов в массиве. ')
    print(my_array.buffer_info())
    print(f'массив {my_array}')
    print(f'Проверка количество вхождений 5 с помощью метода count() - {my_array.count(5)}')
    my_char_array = array('u', ['g', 'e', 'e', 'k'])
    print(f'массив {my_char_array}')
    print(f'Преобразовать массив в строку, используя метод tounicode() - {my_char_array.tounicode()}')
    print(f'массив {my_array}')
    print(f'Преобразовать массив в список, используя метод tolist() - {my_array.tolist()}')
    print('{:_^50}'.format('_'))

    print('\n{:_^50}'.format(' Работа с модулем enum '))
    ''' Аргумент value является названием перечисления, которое используется для создания представления элементов. 
    Второй аргумент names принимает список названий в перечислении. Если подать одну строку, то она будет разбита по 
    пробельным символам и запятым, а значения будут числами, начиная с 1. '''
    BugStatus = enum.Enum(
        value='BugStatus',
        names=('fix_released fix_committed in_progress '
               'wont_fix invalid incomplete new'),
    )

    print('\nMember: {}'.format(BugStatus.new))

    print('\nAll members:')
    for status in BugStatus:
        print('{:15} = {}'.format(status.name, status.value))


if __name__ == '__main__':
    main()
