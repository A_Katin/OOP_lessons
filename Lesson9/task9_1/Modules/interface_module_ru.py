from . import files_operations as file_todo
import sys


def get_ru_menu(dict_in):
    num_todo = int()
    while num_todo != 5:
        print('\n{:*^60}'.format('*') + '\n{:*^60}'.format(' База данных онлайн новостных медиа ') +
              '\n{:|^60}'.format('|') +
              '\n{:|^60}\n'.format(f' На текущий момент база содержит {len(dict_in)} записей ') +
              '\n{:30}'.format(f'1. Все записи') +
              '\n{:30}'.format(f'2. Добавить запись') +
              '\n{:30}'.format(f'3. Детальнее о записи') +
              '\n{:30}'.format(f'4. Сменить язык меню')
              )
        print('\n{:30}'.format(f'5. Завершить работу'))
        choice = int(input('\nВыберите дальнейшее действие: '))
        match choice:
            case 1:
                print('\n+' + '{:+^61}'.format('+') + '+')
                for word, link_ in sorted(dict_in.items()):
                    slash_on_end = '' if link_[-1::] == '/' else '/'
                    print('|' + f"   {word.upper() if len(word) <= 4 else word.capitalize():10}  |    https://{link_}"
                                f"{slash_on_end} ")
                print()
            case 2:
                new_abc = input('добавить короткое имя: ').lower()
                new_url = input('Ссылка: ').strip()
                dict_in[new_abc] = new_url + '/'
                file_todo.dump_to_disk(dict_in)
            case 3:
                find_abc = input('Найти короткое имя: ').lower()
                print(f'\nОнлайн новостной ресурс: {find_abc.upper() if len(find_abc) <= 4 else find_abc.capitalize():<16} '
                      f'Ссылка: https://{dict_in[find_abc]}') if dict_in.get(find_abc) else print(
                    '\nданные не обнаружены')
            case 4:
                try:
                    depth = 1
                    frame = sys._getframe(depth)
                    while frame.f_code.co_name != "<module>":
                        frame = sys._getframe(depth)
                        depth += 1
                    frame.f_globals['main']()
                except ValueError:
                    raise Exception("Не получилось определить вызывающий функцию модуль.")
            case 5:
                file_todo.dump_to_disk(dict_in)
                exit()
            case _:
                print('некорректные данные')
                file_todo.dump_to_disk(dict_in)
                exit()
