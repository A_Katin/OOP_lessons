from . import files_operations as file_todo


def get_multy_lang_menu(dict_in):
    menu_point = 0
    while menu_point != 4:
        print('\n{:|^60}\n'.format(f' Выберите язык интерфейса / Select interface language / Оберіть мову інтерфейсу') +
              '\n{:30}'.format(f'1. En | English') +
              '\n{:30}'.format(f'2. Ru | Russian') +
              '\n{:30}'.format(f'3. Ua | Ukrainian')
              )
        print('\n{:30}'.format(f'4. Exit'))
        menu_point = int(input('\nSelect / Выбор / Вибір : '))
        match menu_point:
            case 1:
                num_todo = int()
                while num_todo != 5:
                    print('\n{:*^60}'.format('*') + '\n{:*^60}'.format(' Database of online media news ') +
                          '\n{:|^60}'.format('|') +
                          '\n{:|^60}\n'.format(f' Now database contain {len(dict_in)} records ') +
                          '\n{:30}'.format(f'1. All records') +
                          '\n{:30}'.format(f'2. Add record') +
                          '\n{:30}'.format(f'3. More about record') +
                          '\n{:30}'.format(f'4. Change menu language')
                          )
                    print('\n{:30}'.format(f'5. Exit'))
                    choice = int(input('\nSelect next action: '))
                    match choice:
                        case 1:
                            print('\n+' + '{:+^61}'.format('+') + '+')
                            for word, link_ in sorted(dict_in.items()):
                                slash_on_end = '' if link_[-1::] == '/' else '/'
                                print(
                                    '|' + f"   {word.upper() if len(word) <= 4 else word.capitalize():10}  |"
                                          f"    https://{link_}"
                                          f"{slash_on_end} ")
                            print()
                        case 2:
                            new_abc = input('Add short name: ').lower()
                            new_url = input('Link: ').strip()
                            dict_in[new_abc] = new_url + '/'
                            file_todo.dump_to_disk(dict_in)
                        case 3:
                            find_abc = input('Find short name: ').lower()
                            print(
                                f'\nOnline news resource: {find_abc.upper() if len(find_abc) <= 4 else find_abc.capitalize():<16} '
                                f'Link: https://{dict_in[find_abc]}') if dict_in.get(find_abc) else print(
                                '\ndata not found')
                        case 4:
                            file_todo.dump_to_disk(dict_in)
                            break
                        case 5:
                            file_todo.dump_to_disk(dict_in)
                            exit()
                        case _:
                            print('incorrect data')
                            file_todo.dump_to_disk(dict_in)
                            exit()
            case 2:
                num_todo = int()
                while num_todo != 5:
                    print('\n{:*^60}'.format('*') + '\n{:*^60}'.format(' База данных онлайн новостных медиа ') +
                          '\n{:|^60}'.format('|') +
                          '\n{:|^60}\n'.format(f' На текущий момент база содержит {len(dict_in)} записей ') +
                          '\n{:30}'.format(f'1. Все записи') +
                          '\n{:30}'.format(f'2. Добавить запись') +
                          '\n{:30}'.format(f'3. Детальнее о записи') +
                          '\n{:30}'.format(f'4. Сменить язык меню')
                          )
                    print('\n{:30}'.format(f'5. Завершить работу'))
                    choice = int(input('\nВыберите дальнейшее действие: '))
                    match choice:
                        case 1:
                            print('\n+' + '{:+^61}'.format('+') + '+')
                            for word, link_ in sorted(dict_in.items()):
                                slash_on_end = '' if link_[-1::] == '/' else '/'
                                print(
                                    '|' + f"   {word.upper() if len(word) <= 4 else word.capitalize():10}  |    https://{link_}"
                                          f"{slash_on_end} ")
                            print()
                        case 2:
                            new_abc = input('добавить короткое имя: ').lower()
                            new_url = input('Ссылка: ').strip()
                            dict_in[new_abc] = new_url + '/'
                            file_todo.dump_to_disk(dict_in)
                        case 3:
                            find_abc = input('Найти короткое имя: ').lower()
                            print(
                                f'\nОнлайн новостной ресурс: {find_abc.upper() if len(find_abc) <= 4 else find_abc.capitalize():<16} '
                                f'Ссылка: https://{dict_in[find_abc]}') if dict_in.get(find_abc) else print(
                                '\nданные не обнаружены')
                        case 4:
                            file_todo.dump_to_disk(dict_in)
                            break
                        case 5:
                            file_todo.dump_to_disk(dict_in)
                            exit()
                        case _:
                            print('некорректные данные')
                            file_todo.dump_to_disk(dict_in)
                            exit()
            case 3:
                num_todo = int()
                while num_todo != 5:
                    print('\n{:*^60}'.format('*') + '\n{:*^60}'.format(' База даних онлайн агенцій новин ') +
                          '\n{:|^60}'.format('|') +
                          '\n{:|^60}\n'.format(f' Станом на зараз база налічує {len(dict_in)} записів ') +
                          '\n{:30}'.format(f'1. Усі записи') +
                          '\n{:30}'.format(f'2. Додати запис') +
                          '\n{:30}'.format(f'3. Детальніше про запис') +
                          '\n{:30}'.format(f'4. Змінити мову меню')
                          )
                    print('\n{:30}'.format(f'5. Завершити роботу'))
                    choice = int(input('\nОберіть подальшу дію: '))
                    match choice:
                        case 1:
                            print('\n+' + '{:+^61}'.format('+') + '+')
                            for word, link_ in sorted(dict_in.items()):
                                slash_on_end = '' if link_[-1::] == '/' else '/'
                                print(
                                    '|' + f"   {word.upper() if len(word) <= 4 else word.capitalize():12}  |    https://{link_}"
                                          f"{slash_on_end} ")
                            print()
                        case 2:
                            new_abc = input('Додати коротке і`мя: ').lower()
                            new_url = input('Посилання: ').strip()
                            dict_in[new_abc] = new_url + '/'
                            file_todo.dump_to_disk(dict_in)
                        case 3:
                            find_abc = input('Знайти коротке і`мя: ').lower()
                            print(
                                f'\nОнлайн агенція новин: {find_abc.upper() if len(find_abc) <= 4 else find_abc.capitalize():<16} '
                                f'Посилання: https://{dict_in[find_abc]}') if dict_in.get(find_abc) else print(
                                '\nдані не знайдено')
                        case 4:
                            file_todo.dump_to_disk(dict_in)
                            break
                        case 5:
                            file_todo.dump_to_disk(dict_in)
                            exit()
                        case _:
                            print('некоректні дані')
                            file_todo.dump_to_disk(dict_in)
                            exit()
            case 4:
                file_todo.dump_to_disk(dict_in)
                exit()
            case _:
                print('incorrect data')
                exit()
