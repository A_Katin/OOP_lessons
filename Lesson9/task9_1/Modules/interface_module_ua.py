from . import files_operations as file_todo
import sys


def get_ua_menu(dict_in):
    num_todo = int()
    while num_todo != 5:
        print('\n{:*^60}'.format('*') + '\n{:*^60}'.format(' База даних онлайн агенцій новин ') +
              '\n{:|^60}'.format('|') +
              '\n{:|^60}\n'.format(f' Станом на зараз база налічує {len(dict_in)} записів ') +
              '\n{:30}'.format(f'1. Усє записи') +
              '\n{:30}'.format(f'2. Додати запис') +
              '\n{:30}'.format(f'3. Детальніше про запис') +
              '\n{:30}'.format(f'4. Змінити мову меню')
              )
        print('\n{:30}'.format(f'5. Завершити роботу'))
        choice = int(input('\nОберіть подальшу дію: '))
        match choice:
            case 1:
                print('\n+' + '{:+^61}'.format('+') + '+')
                for word, link_ in sorted(dict_in.items()):
                    slash_on_end = '' if link_[-1::] == '/' else '/'
                    print('|' + f"   {word.upper() if len(word) <= 4 else word.capitalize():12}  |    https://{link_}"
                                f"{slash_on_end} ")
                print()
            case 2:
                new_abc = input('Додати коротке і`мя: ').lower()
                new_url = input('Посилання: ').strip()
                dict_in[new_abc] = new_url + '/'
                file_todo.dump_to_disk(dict_in)
            case 3:
                find_abc = input('Знайти коротке і`мя: ').lower()
                print(f'\nОнлайн агенція новин: {find_abc.upper() if len(find_abc) <= 4 else find_abc.capitalize():<16} '
                      f'Посилання: https://{dict_in[find_abc]}') if dict_in.get(find_abc) else print(
                    '\nдані не знайдено')
            case 4:
                try:
                    depth = 1
                    frame = sys._getframe(depth)
                    while frame.f_code.co_name != "<module>":
                        frame = sys._getframe(depth)
                        depth += 1
                    frame.f_globals['main']()
                except ValueError:
                    raise Exception("Не вдалося визначити модуль, що викликає функцію.")
            case 5:
                file_todo.dump_to_disk(dict_in)
                exit()
            case _:
                print('некоректні дані')
                file_todo.dump_to_disk(dict_in)
                exit()
