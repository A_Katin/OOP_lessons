import pickle
import os
from sys import platform


file_name = ''


def _get_dict_default():
    dic_db = {
        'bbc': 'www.bbc.com/news/world', 'tnyt': 'www.nytimes.com', 'bfnews': 'buzzfeednews.com/section/world',
        'aljazeera': 'aljazeera.com', 'db': 'defense-blog.com', 'gi': 'globalissues.org',
        'tcb': 'thecipherbrief.com', 'cnn': 'edition.cnn.com/world', 'wp': 'washingtonpost.com/world',
        'cnbc': 'cnbc.com/world-top-news', 'reuters': 'reuters.com', 'dw': 'www.dw.com', 'cbc': 'cbc.ca/news',
        'time': 'time.com/', 'unian': 'www.unian.ua/', 'unn': 'www.unn.com.ua/'}

    return dic_db


def get_slash():
    if platform in ["linux", "linux2", "darwin"]:
        return r'\\'
    else:
        return r'/'


def get_database_dict():
    global file_name
    dict_in = {}
    # корректируем имя файла в зависимости от платформы
    file_name = 'Modules' + get_slash() + '_files' + get_slash() + 'file_db'
    # проверяем существование файла и беспроблемного доступа к нему
    # отсюда получаем словарь
    if os.path.exists(file_name) and os.access(file_name, os.R_OK):
        with open(file_name, 'rb') as f_in:
            dict_in = pickle.load(f_in)
    if len(dict_in) == 0:
        dict_in = _get_dict_default()

    return dict_in


def dump_to_disk(dict_to_disk):
    with open(file_name, 'wb') as f_out:
        pickle.dump(dict_to_disk, f_out)
