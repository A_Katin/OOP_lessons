# сервіс для скорочення посилань) таким чином, щоб у нього була основна частина,
# яка відповідала би за логіку роботи та надавала узагальнений інтерфейс, і модуль представлення,
# який відповідав би за взаємодію з користувачем. При заміні останнього на інший,
# який взаємодіє з користувачем в інший спосіб, програма має продовжувати коректно працювати.
from Modules import files_operations as file_todo
from Modules import interface_module_multy_lang as multy_lang_menu


main_dict = {}


def main():
    global main_dict
    menu_point = 0
    main_dict = file_todo.get_database_dict()
    try:
        while True:
            multy_lang_menu.get_multy_lang_menu(main_dict)
    except Exception as ex:
        # что-то пошло не так
        print(ex)
        # запасной языковой выбор и инициализация меню из соответствующего модуля
        from Modules import interface_module_en as en_menu
        from Modules import interface_module_ru as ru_menu
        from Modules import interface_module_ua as ua_menu
        while menu_point != 4:
            print('\n{:|^60}\n'.format(
                f' Выберите язык интерфейса / Select interface language / Оберіть мову інтерфейсу') +
                  '\n{:30}'.format(f'1. En | English') +
                  '\n{:30}'.format(f'2. Ru | Russian') +
                  '\n{:30}'.format(f'3. Ua | Ukrainian')
                  )
            print('\n{:30}'.format(f'4. Exit'))
            choice = int(input('\nSelect / Выбор / Вибір : '))
            match choice:
                case 1:
                    en_menu.get_en_menu(main_dict)
                case 2:
                    ru_menu.get_ru_menu(main_dict)
                case 3:
                    ua_menu.get_ua_menu(main_dict)
                case 4:
                    file_todo.dump_to_disk(main_dict)
                    exit()
                case _:
                    print('incorrect data')
                    file_todo.dump_to_disk(main_dict)
                    exit()


if __name__ == '__main__':
    main()
