# Створіть модуль для отримання простих чисел. Імпортуйте його з іншого модуля. Імпортуйте його окремі імена.
import primes


def main():
    a_count = int(input('Введите натуральное число ряда: '))
    print(f'переменная модуля primes j {primes.j}')
    print(primes.get_primes(a_count))


if __name__ == '__main__':
    main()